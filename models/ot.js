/**
 * Created by dau on 30.03.2016.
 */

module.exports = function(sequelize, DataTypes) {

    //var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var ot = sequelize.define('ot', {
        knom: DataTypes.INTEGER,
        ln: DataTypes.STRING,
        pred: DataTypes.STRING,
        tek: DataTypes.STRING,
        predn: DataTypes.STRING,
        tekn: DataTypes.STRING,
        jeu: DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
        //norm: DataTypes.DECIMAL(1,0), //нужно ли это?
        //nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)

    },{
        getterMethods   : {
            vidnach     : function()  { return '08' }, //вид начислений, для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },
        classMethods: {
            associate: function(models) {
                ot.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom
                ot.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu',
                    scope: {
                        gm: gm
                    }
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_ot',//+mmyy,
        schema: 'lice'
    });
    ot.removeAttribute('id');
    return ot;
}
