/**
 * Created by dau on 06.04.2015.
 * model from gilcom database
 */

module.exports = function(sequelize, DataTypes) {
    var haracter = sequelize.define('haracter', {
        knom: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        jeu: DataTypes.INTEGER,
        exp_2: DataTypes.STRING
    },{
        timestamps: false,
        freezeTableName: true,
        tableName: 'haracter',
        schema: 'lice'
    });
    haracter.removeAttribute('id');
    /*
     haracter.hasMany(en0215, {
     foreignKey: 'knom'
     });
     */
    return haracter;
}
