/**
 * Created by dau on 06.05.2015.
 */
//модуль Загружающий все модели и связи между ними

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
//var env       = process.env.NODE_ENV || "development";
//var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.postgres.gilcom_connstring,{
    logging: winston.info,
    logging: console.log
});

module.exports = function(reload) {

    var db = {};
    if (typeof reload !== 'undefined') { //Удаляем кешированные в sequelize модели если нужна перезагрузка
        sequelize.importCache = {};
    }
    fs
        .readdirSync(__dirname)
        .filter(function (file) {
            return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "user.js")&& (file !== "sql"); //модель user, sql - имею т неподходящую для автоматического импорта структуру
        })
        .forEach(function (file) {//цикл по файлам в каталоге моделей
            if (typeof reload !== 'undefined') { //Удаляем кешированные в node модели если нужна перезагрузка
                delete require.cache[require.resolve(path.join(__dirname, file))];
            }
            var model = sequelize.import(path.join(__dirname, file)); //тут импорт модели определенной в другом файле
            db[model.name] = model;
        });
//установка связей между моделями
    Object.keys(db).forEach(function (modelName) {
        if ("associate" in db[modelName]) {
            db[modelName].associate(db);
        }
    });

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;

    return db
}
//module.exports = db;
