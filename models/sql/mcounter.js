/**
 * Created by dau on 09.11.2015.
 * модели в виде SQL запросов
 */

module.exports.allcounters = function(counter_type, mail_type,counterType) {
    switch(counterType) {   //Электроэнергия и гор вода с лицевыми начинающимися на 9 - на сайт не выкладывать
        case 'gvod':
            var no9999 ='AND cnt.jeu != 9999';
            break;
        case 'ener':
            var no9999 =' AND cnt.jeu != 9999';
           // var noTsgPr_domtsg = "ts.pr_dom !=2"
            break;
        default:
            var no9999 ='';
    }
    return  "select distinct har.exp_2 ,\
                cnt.pred as pred, cnt.tek as tek, cnt.predn as predn, cnt.tekn as tekn,\
                cnt.ln, cnt.nkw1, cnt.nkw2 from "+counter_type+" cnt\
                left join otch.tsg ts on cnt.jeu = ts.jeu and ts.gm=$2 and ts.ot_musz = $1 and ts."+mail_type+"\
                left join otch.tsg ts99 on cnt.knom = ts99.knom and cnt.jeu=9999 and ts99.gm=$2 and ts99.ot_musz = $1 and ts99.jeu<2000\
                left join lice.haracter har on har.knom = cnt.knom\
                where ( ts.ot_musz = $1 or ts99.ot_musz = $1 ) "+no9999+"  ORDER BY har.exp_2, nkw1, nkw2, ln";

};


//показания общедомовых счетчиков
module.exports.countcommons = function(counter_type, ot_musz, gm, counterTableOdn, mail_type, counter_type_older,counterType) {
    switch(counterType) {   //Электроэнергия с opu_elen ='' не выкладывать
        case 'en':
            var noOpuElen ="Left JOIN tar.ot_musz mz ON  mz.ot_musz = wh.ot_musz WHERE trim(mz.opu_elen)!=''; ";
            break;
        default:
            var noOpuElen ='';
    }

    var cSQL1 = " CREATE TEMP TABLE whole_home ON COMMIT DROP AS \
                     select  har.exp_2, cnt.knom, \
                        cnt.pred as pred, cnt.tek as  tek, cnt.predn as predn, \
                        cnt.tekn as tekn, ts.ot_musz \
                        from " + counter_type + " cnt\
                        left join otch.tsg ts on cnt.jeu = ts.jeu \
                        left join lice.haracter har on har.jeu = cnt.jeu \
                        where ts.ot_musz = " + ot_musz + " and ts.gm=" + gm + ";";
//сумма по дому (tek-pred)+(tekn-predn)
    var cSQL2 = " CREATE TEMP TABLE sum_for_odn ON COMMIT DROP AS \
                      select cnt.knom, \
                        sum(CASE WHEN cnt.tek - cnt.pred<0 then 0 else cnt.tek - cnt.pred end + CASE WHEN cnt.tekn - cnt.predn<0 then 0 else cnt.tekn - cnt.predn end) \
                         as odn \
                        from " + counterTableOdn + " cnt \
                        left join otch.tsg ts on cnt.jeu = ts.jeu \
                        where ts." + mail_type + " and ts.ot_musz = " + ot_musz + " and ts.gm=" + gm + " \
                        group by cnt.knom; ";
    var cSQL3 = " select wh.*, sm.odn as odn, \
                           case\
                            when sm.odn + COALESCE(cto.nordomn,0) = 0 then 0 \
                            else\
                            round((wh.tek - wh.pred + wh.tekn - wh.predn)/(sm.odn + COALESCE(cto.nordomn,0)),4) end as koeff, \
                            COALESCE(cto.nordomn,0) as nordom \
                         from whole_home wh  \
                         left join sum_for_odn sm on wh.knom = sm.knom\
                         left join " + counter_type_older + " cto on cto.knom = wh.knom "+noOpuElen;
    console.log(cSQL1 + cSQL2 + cSQL3);
    return cSQL1 + cSQL2 + cSQL3;

}
// заготовки сложных запросов

//объединение всех счетчиков в одну строку по ln
/* var cSQL = "select har.exp_2 , COALESCE(ener.ln, gvod.ln, hvod.ln) as ln, COALESCE(ener.nkw1, gvod.nkw1, hvod.nkw1) as nkw1,\
 COALESCE(ener.nkw2, gvod.nkw2, hvod.nkw2) as nkw2,\
 ener.pred as ener_pred, ener.tek as ener_tek, gvod.pred as gvod_pred, gvod.tek as gvod_tek,\
 hvod.pred as hvod_pred, hvod.tek as hvod_tek\
 from lice.meter_ener0115 ener\
 FULL OUTER JOIN lice.meter_gvod0115 gvod on gvod.ln=ener.ln\
 FULL OUTER JOIN lice.meter_hvod0115 hvod on hvod.ln=ener.ln\
 LEFT JOIN otch.tsg ts on ts.jeu = COALESCE(ener.jeu, gvod.jeu, hvod.jeu)\
 LEFT JOIN lice.haracter har on har.jeu = ts.jeu\
 WHERE ts.ot_musz = $1 and gm = $2 ";
 */

/*
 var cSQL = "SELECT substring(TABLE_NAME from 11 for 4) as Table_name_date,\
 substring(TABLE_NAME from 11 for 2)||'.'||'20'||substring(TABLE_NAME from 13 for 2) as Table_name_f_date,\
 substring(TABLE_NAME from 13 for 2)||substring(TABLE_NAME from 11 for 2)  as for_order\
 FROM information_schema.tables\
 WHERE table_type = 'BASE TABLE'\
 and table_schema NOT IN ('pg_catalog', 'information_schema')\
 and TABLE_NAME='meter_ener0115'\
 ORDER BY for_order DESC LIMIT 1;"
 */
/*
 select 9 as vidnach, lpad(ts.jeu::varchar,4) as rjeu, substring(ts.gm::varchar from 5 for 2)as rmes,
 substring(ts.gm::varchar from 1 for 4)as rgod,
 cnt.ln, lpad(cnt.nkw1::varchar,4) as kw1, lpad(cnt.nkw2::varchar,4) as kw2,
 lpad(cnt.pred::varchar,12) as rpred, lpad(cnt.tek::varchar,12) as rtek, 1 as kwt,
 '00000000000000.0000' as rgk,'00' as flag, lpad(ts.ot_musz::varchar,3),
 '                 ' as space, '000000000000.0000' as sum_norm, lpad(har.exp_2,80) as address
 from  lice.meter_ener0315  cnt
 left join otch.tsg ts on cnt.jeu = ts.jeu
 left join lice.haracter har on har.jeu = cnt.jeu
 where  ts.gm=201503 and cnt.tek > 0
 */