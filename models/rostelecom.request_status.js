/**
 * Created by dau on 14.09.2016.
 */
// establish a connection to the database.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.postgres.rostelecom_connstring);

// create a model, and sync that model with the database.
module.exports = function() {
    var request_status = sequelize.define('request_status', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            payStatus:{
                type: Sequelize.STRING
            },
            description:{
                type: Sequelize.STRING
            }

        }, {
            freezeTableName: true,
            tableName: 'request_status',
            timestamps: false
        }
    );
    return request_status;
}

