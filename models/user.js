/**  Created by dau on 09.04.2015
*    model user
*/
// establish a connection to the database.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.postgres.users_connstring,{logging: false

});

// create a model, and sync that model with the database.
module.exports = function() {
    var User = sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        ot_musz:{type:Sequelize.INTEGER,
                validate:{
                    isInt:{msg:'ot_musz должно быть целое число'}
                }
        },
        username:{
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,

            validate: {

                isUnique: function() {
                    //если индекс пустой - меняем условие поиска, иначе будет ошибка
/*
                    if (this.id == null||this.id.length == 0){
                        var idcond = null
                    }else{
                        var idcond = {ne: this.id}
                    }
*/
                    //var self = this;
                    var condition = {username: this.username,id:parseInt(this.id)};
                    return User.find({ where: {username: condition.username}})
                        .then(function(result) {

                            if (result && condition.id !== result.id) {
                                // We found a user with this login.
                                throw new Error('Такой логин уже используется!');
                            }
                        });
                }

            }

        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        role: Sequelize.STRING,
        email:{
            type: Sequelize.STRING,
            allowNull: true,
            validate:  {
                isEmail:{ msg:'Это не email'}
            }
        },
        name: Sequelize.STRING,
        deleted: Sequelize.BOOLEAN,
        phone: Sequelize.STRING

    },{defaultScope: { where: { deleted: 'FALSE' }}} //не показывать удаленных пользователей
    );
    return User;
}

