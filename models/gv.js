/**
 * Created by dau on 07.05.2015.
 */
module.exports = function(sequelize, DataTypes) {
   // var typeCounter = 'gv';
   // var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var gv = sequelize.define('gv', {
        knom: DataTypes.INTEGER,
        ln: DataTypes.STRING,
        pred: DataTypes.STRING,
        tek: DataTypes.STRING,
        predn: DataTypes.STRING,
        tekn: DataTypes.STRING,
        jeu: DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
       // norm: DataTypes.DECIMAL(1,0), //нужно ли это?
       // nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)

    },{
        getterMethods   : {
            vidnach     : function()  { return '06' }, //вид начислений, для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },
        classMethods: {
            associate: function(models) {
                gv.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom
                gv.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu',
                    scope: {
                        gm: gm
                    }
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_gv',//+mmyy,
        schema: 'lice'
    });
    gv.removeAttribute('id');
    return gv;
}