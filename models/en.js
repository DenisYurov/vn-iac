/**
 * Created by dau on 07.05.2015.
 */
//модель общедомового счетчика энергии
module.exports = function(sequelize, DataTypes) {
   // var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var en = sequelize.define('en', {
        knom: DataTypes.INTEGER,
        ln: DataTypes.INTEGER,
        pred: DataTypes.DECIMAL(14, 4),
        tek: DataTypes.DECIMAL(14, 4),
        predn: DataTypes.DECIMAL(14, 4),
        tekn: DataTypes.DECIMAL(14, 4),
        jeu: DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
       // norm: DataTypes.DECIMAL(1,0), //нужно ли это?
       // nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)

    },{
        getterMethods   : {
            vidnach     : function()  { return '09' }, //вид начислений, для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },
        classMethods: {
            associate: function(models) {
                en.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom
                en.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu',
                    scope: {
                        gm: gm
                    }
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_en',//+mmyy,
        schema: 'lice'
    });
    en.removeAttribute('id');
    return en;
}
//en.belongsTo(haracter, { foreignKey: 'knom' });