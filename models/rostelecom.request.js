/**
 * Created by dau on 03.02.2016.
 */
/**
 * Created by dau on 02.02.2016.
 */
// establish a connection to the database.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.postgres.rostelecom_connstring,{

});

// create a model, and sync that model with the database.
module.exports = function() {
    var Request = sequelize.define('request', {
            request_id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            reqTime:{
                type: Sequelize.STRING
            },
            reqStatus: {
                type: Sequelize.INTEGER
            },
            payStatus: {
                type: Sequelize.INTEGER
            },
            reqNote: Sequelize.STRING,
            payment_id: Sequelize.INTEGER
        }, {
            freezeTableName: true,
            tableName: 'request',
            timestamps: false
        }
    );
    return Request;
}

