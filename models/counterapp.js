/**
 * Created by dau on 07.05.2015.
 */
//model application settings
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('counterapp', {
        isclosednow: DataTypes.BOOLEAN,
        daywhenclose: DataTypes.INTEGER,
        emailaddr1: DataTypes.STRING,
        emailpassword1:DataTypes.STRING
    },{
        timestamps: false,
        freezeTableName: true,
        tableName: 'counterapp'
    });

};