/**
 * Created by dau on 07.05.2015.
 */
module.exports = function(sequelize, DataTypes) {
    var ot_musz = sequelize.define('ot_musz', {
        ot_musz: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: DataTypes.STRING,
        namefile:DataTypes.STRING,
        e_mail: DataTypes.STRING,
        tel: DataTypes.STRING,
        mail_met: DataTypes.DECIMAL(1),
        ener: DataTypes.DECIMAL(1),
        opu_elen: DataTypes.STRING,
        psw: DataTypes.STRING
    },{
        timestamps: false,
        freezeTableName: true,
        tableName: 'ot_musz',
        schema: 'tar'
    });
    ot_musz.removeAttribute('id');
    return ot_musz;
}