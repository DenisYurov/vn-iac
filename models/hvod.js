/**
 * Created by dau on 07.05.2015.
 */
module.exports = function(sequelize, DataTypes) {
    var typeCounter = 'hvod';
   // var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var hvod = sequelize.define('hvod', {
        knom: DataTypes.INTEGER,
        ln: {type:DataTypes.INTEGER,
        primaryKey: true},
        pred: DataTypes.STRING,
        tek: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего первого счетчика холодной воды должны быть числом"}
            }
        },
        predn: DataTypes.STRING,
        tekn: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего второго счетчика холодной воды должны быть числом"}
            }
        },
        nkw1: DataTypes.STRING,
        nkw2: DataTypes.STRING,
        jeu: DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
       // norm: DataTypes.DECIMAL(1,0), //нужно ли это?
       // nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)

    },{
        getterMethods   : {         // вычисляемое поле
            vidnach     : function()  { return '05' }, //вид начислений, для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },
        classMethods: {             //связи с другими моделями
            associate: function(models) {
                hvod.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom
                hvod.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu'/*,
                    scope: {
                        gm: gm
                    }
                    */
                });
                hvod.belongsTo(                                         //связь с tsg c условием mail9: '0' и, само собой gm = текущая дата
                    models.tsg,
                    {  as: 'tsgipu' ,
                       foreignKey: 'jeu',
                       scope: {
                            mail5: '0',
                            gm: gm
                       }
                });
                hvod.belongsTo(                                         //связь с tsg для jeu=9999, ln =9999 jeu<2000  и, само собой gm = текущая дата
                    models.tsg,
                    {  as: 'tsg99' ,
                        foreignKey: 'knom',
                        targetKey:'knom',
                        scope: {
                            jeu: {
                                lte: 2000
                            },
                            gm: gm
                        }
                    });

            }
        },
        validate: {             //общие проверки между полями модели
            hvod_tek: function () {
                if ((+this.pred > +this.tek) && +this.tek > 0) {
                    throw new Error('Предыдущие показания первого счетчика холодной воды должены быть меньше текущего')
                }
            },
            hvod_tekn: function () {
                if ((+this.predn > +this.tekn) && +this.tekn > 0) {
                    throw new Error('Предыдущие показания второго счетчика холодной воды должены быть меньше текущего')
                }
            }
        },
        instanceMethods: {
            validateWarn: function(attr) {
                var msg=[];
                if (((attr.tek||this.tek) - (attr.pred||this.pred))>70){
                    var ms ={};
                    ms.type = "Validation warning",
                        ms.message = "Вы уверены что холодная вода текущие первый = " + this.tek +' ?';
                    ms.path='ener_tek'
                    msg.push(ms);
                };
                if (((attr.tekn||this.tekn) - (attr.predn||this.predn))>70){
                    var ms ={};
                    ms.type = "Validation warning",
                        ms.message = "Вы уверены что холодная вода текущие второй = " + this.tek +' ?';
                    ms.path='ener_tekn'
                    msg.push(ms);
                }
                return msg
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_hvod',//+mmyy,
        schema: 'lice'
    });
    hvod.removeAttribute('id');
    return hvod;
}