/**
 * Created by dau on 07.05.2015.
 */

module.exports = function(sequelize, DataTypes) {
    //var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var ener = sequelize.define('ener', {
        knom: DataTypes.INTEGER,
        ln: {type:DataTypes.INTEGER,
            primaryKey: true},
        pred: {
           type:  DataTypes.DECIMAL(14, 4),
           validate: {
                isDecimal: {msg:"Показания предидущего дневного счетчика энергии должны быть числом1"}
           }
        },
        tek: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего дневного счетчика энергии должны быть числом"}
            }
        },
        predn: {
            type: DataTypes.DECIMAL(14, 4),
            validate: {
                isDecimal: {msg:"Показания предидущего ночного счетчика энергии должны быть числом"}
            }
        },
        tekn: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего ночного счетчика энергии должны быть числом"}
            }
        },
        nkw1: DataTypes.STRING,
        nkw2: DataTypes.STRING,
        jeu:  DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
       // norm: DataTypes.DECIMAL(1,0), //нужно ли это?
       // nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)
    },{
        getterMethods   : {
            vidnach     : function()  { return '09' }, //вид начислений, нужно для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },
        classMethods: {
                                    //тут определяем связи с другими моделями
            associate: function(models) {
                ener.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom

                ener.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu'//,
                    /*
                    scope: {
                        gm: gm
                    }
                    */
                });

                ener.belongsTo(                                         //связь с tsg c условием mail9: '0' и, само собой gm = текущая дата. Для индивидуальных счетчиков
                    models.tsg,{
                       as: 'tsgipu' ,
                       foreignKey: 'jeu',

                        scope: {
                            mail9: '0',
                            gm: gm
                       }

                });
                ener.belongsTo(                                         //связь с tsg для jeu=9999, ln =9999 jeu<2000  и, само собой gm = текущая дата
                    models.tsg,
                    {  as: 'tsg99' ,
                        foreignKey: 'knom',
                        targetKey:'knom',
                        scope: {
                            jeu: {
                                lte: 2000
                            },
                            gm: gm
                        }
                    });

            }
        },
        validate: {                 // проверки между полями модели
            ener_tek: function () {
                if ((+this.pred > +this.tek) && +this.tek > 0) {
                    throw new Error('Предыдущие дневные показания счетчика энергии должены быть меньше текущего')
                    //return next('Предидущий день должен быть меньше текущего день');
                }
            },
            ener_tekn: function () {
                if ((+this.predn > +this.tekn) && +this.tekn > 0) {
                    throw new Error('Предыдущие ночные показания счетчика энергии должены быть меньше текущего')
                    //return next('Предидущий день должен быть меньше текущего день');
                }
            }
        },
        instanceMethods: {
            validateWarn: function(attr) {
                var msg=[];
                if (((attr.tek||this.tek) - (attr.pred||this.pred))>1500){
                    var ms ={};
                    ms.type = "Validation warning",
                    ms.message = "Вы уверены что электроэнергия текущие день = " + this.tek +' ?';
                    ms.path='ener_tek'
                    msg.push(ms);
                };
                if (((attr.tekn||this.tekn) - (attr.predn||this.predn))>500){
                    var ms ={};
                    ms.type = "Validation warning",
                        ms.message = "Вы уверены что электроэнергия текущие ночь = " + this.tek +' ?';
                    ms.path='ener_tekn'
                    msg.push(ms);
                }
                return msg
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_ener',//+mmyy,
        schema: 'lice'
    }, { //Електроэнергия и гор вода с лицевыми начинающимися на 9 - на сайт не выкладывать
        defaultScope: {
            where: {
                jeu:{$ne: 9999}
            }
        }
    });
    ener.removeAttribute('id');
    return ener;
}
