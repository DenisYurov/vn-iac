/**
 * Created by dau on 02.02.2016.
 */
// establish a connection to the database.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.postgres.rostelecom_connstring,{

});

// create a model, and sync that model with the database.
module.exports = function() {
    var payment = sequelize.define('payment', {
            payment_id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            payTime:{
                type: Sequelize.STRING
            },
            svcNum: {
                type: Sequelize.STRING,
                allowNull: false
            },
            payAmount: Sequelize.STRING,
            status_id: Sequelize.INTEGER,
            lastReqNote: Sequelize.STRING
        },{ classMethods: {
            associate: function (models) {
                payment.belongsTo(models.payment_status, {foreignKey: 'status_id'}); //left join с payment_status по полю status_id
            }
        },
            freezeTableName: true,
            tableName: 'payment',
            timestamps: false
        }
    );
    return payment;
}

