/**
 * Created by dau on 07.05.2015.
 */
module.exports = function(sequelize, DataTypes) {
    //if (typeof gm == 'undefined'){
    var gm = moment().format("YYYYMM");
    //}
    //Определение модели
    var tsg = sequelize.define('tsg', {
        knom: DataTypes.INTEGER,
        jeu: {
            type:DataTypes.INTEGER,
            primaryKey: true
        },
        gm: DataTypes.STRING,
        ot_musz: DataTypes.INTEGER,

        mail5: DataTypes.INTEGER,
        mail6: DataTypes.INTEGER,
        mail8: DataTypes.INTEGER,
        mail9: DataTypes.INTEGER
    },{
        defaultScope: {
            where: {
                gm: gm
            }
        },

        timestamps: false,
        freezeTableName: true,
        tableName: 'tsg',
        schema: 'otch'
    });
    tsg.removeAttribute('id');

    return tsg;
}