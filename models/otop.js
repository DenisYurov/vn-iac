/**
 * Created by dau on 12.11.2015.
 */
module.exports = function(sequelize, DataTypes) {
   // var mmyy = moment().format("MMYY");
    var gm = moment().format("YYYYMM");
    var otop = sequelize.define('otop', {
        knom: DataTypes.INTEGER,
        ln: {type:DataTypes.INTEGER,
            primaryKey: true},
        pred: DataTypes.STRING,
        tek: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего счетчика отопления должны быть числом"}
            }
        },
        predn: DataTypes.STRING,
        tekn: {
            type: DataTypes.DECIMAL(14, 4),
            allowNull: true,
            validate: {
                isDecimal: {msg:"Показания текущего второго счетчика отопления должны быть числом"}
            }
        },
        nkw1: DataTypes.STRING,
        nkw2: DataTypes.STRING,
        jeu: DataTypes.INTEGER,
        sayt: DataTypes.INTEGER,// признак показа счетчика на сайте
        //norm: DataTypes.DECIMAL(1,0), //нужно ли это?
        //nordom: DataTypes.DECIMAL(14,4) //нужно ли это?
        nordomn: DataTypes.DECIMAL(14,4)
    },{
        getterMethods   : {
            vidnach     : function()  { return '08' }, //вид начислений, нужно для выгрузки счетчиков
            pred:function()  { return parseFloat(this.getDataValue('pred')).toFixed(4) },
            tek:function()  { return parseFloat(this.getDataValue('tek')).toFixed(4) },
            predn:function()  { return parseFloat(this.getDataValue('predn')).toFixed(4) },
            tekn:function()  { return parseFloat(this.getDataValue('tekn')).toFixed(4) }
        },

        classMethods: {
            //тут определяем связи с другими моделями
            associate: function(models) {
                otop.belongsTo(models.haracter, { foreignKey: 'knom' }); //left join с haracter по полю knom

                otop.belongsTo(models.tsg, {                             //left join с tsg по полю jeu и поле gm = текущая дата
                    foreignKey: 'jeu'/*,
                    scope: {
                        gm: gm
                    }
                    */
                });
                otop.belongsTo(                                         //связь с tsg c условием mail8: '0' и, само собой gm = текущая дата. Для индивидуальных счетчиков
                    models.tsg,{
                        as: 'tsgipu' ,
                        foreignKey: 'jeu',
                        scope: {
                            mail8: '0',
                            gm: gm
                        }
                });
                otop.belongsTo(                                         //связь с tsg для jeu=9999, ln =9999 jeu<2000  и, само собой gm = текущая дата
                    models.tsg,
                    {  as: 'tsg99' ,
                        foreignKey: 'knom',
                        targetKey:'knom',
                        scope: {
                            jeu: {
                                lte: 2000
                            },
                            gm: gm
                        }
                    });

            }
        },
        validate: {                 // проверки между полями модели
            otop_tek: function () {
                if ((+this.pred > +this.tek) && +this.tek > 0) {
                    throw new Error('Предыдущие дневные показания счетчика отопления должены быть меньше текущего')
                }
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'meter_otop',//+mmyy,
        schema: 'lice'
    });
    otop.removeAttribute('id');
    return otop;
}
