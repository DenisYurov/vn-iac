/**
 * Created by dau on 28.03.2016.
 * payment_status model
 */
// establish a connection to the database.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.postgres.rostelecom_connstring,{

});

// create a model, and sync that model with the database.
module.exports = function() {
    var Payment_status = sequelize.define('payment_status', {
            status_id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
           name:{
                type: Sequelize.STRING
            }
        }, {
            freezeTableName: true,
            tableName: 'payment_status',
            timestamps: false
        }
    );
    return Payment_status;
}

