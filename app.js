//  made by Denis Yurov
var express = require('express'),
    exphbs  = require('express-handlebars'), // "express3-handlebars"
    app = express();

// подключаем конфигурационный файл
GLOBAL.config = require('./config')
//описание соединения с SQL сервером
var pg = require("pg");
GLOBAL.client = new pg.Client(config.postgres.gilcom_connstring);

// Sequelize модель базы. Create a new instance of Sequelize
//GLOBAL.Sequelize = require('sequelize');

// hb as template engine
app.engine('handlebars', exphbs({defaultLayout: 'main',layoutsDir:__dirname + '/views/layouts'}));
app.set('view engine', 'handlebars');
app.set('views', __dirname + '/views');

// serve static files
app.use('/node',express.static(__dirname + '/public'));
//запоминаем путь к корневой директории приложения
var path = require('path');
GLOBAL.appRoot = path.resolve(__dirname);

//                                               настройка журналов используем логгер winston
//GLOBAL.winston = require('winston');
var logger = require('winston');
logger.exitOnError = false;
// журнал событий ведем в базе gilcom
var PostgreSQL = require("winston-postgresql").PostgreSQL;
//winston.emitErrs = true;
//журнал только для http запросов
var expressWinston = require('express-winston');

//winston.add(PostgreSQL,toptions);
app.use(expressWinston.logger({
    transports: [
        new logger.transports.PostgreSQL({
            "connString": config.postgres.gilcom_connstring,
        //  "customSQL": "select custom_logging_function($1, $2, $3)",
            "tableName": config.log.httptablename
        })
    ]
}));


// простой winston
GLOBAL.winston = new (logger.Logger)({
    transports: [
        new (logger.transports.PostgreSQL)({
            "connString": config.postgres.gilcom_connstring,
            "tableName": config.log.sqltablename
        }),
        new (logger.transports.File)({
            name: 'error-file',
            filename: path.join(GLOBAL.appRoot, config.log.errfilename),
            level: 'error'
        })
    ],
    exceptionHandlers:[
        new (logger.transports.Console)({
            json: true,
            humanReadableUnhandledException: true,
            handleExceptions: true
        }),
        new logger.transports.PostgreSQL({
            "connString": config.postgres.gilcom_connstring,
            "tableName": config.log.errtablename
        })
    ]
});

//                                          закончили настраивать логгер winston

// module for date operation
GLOBAL.moment = require('moment');
moment().format();

// localisation
moment.locale('ru', {
    months : [
        "январь", "февраль", "март", "апрель", "май", "июнь", "июль",
        "август", "сентябрь", "октябрь", "ноябрь", "декабрь"
    ]
});
moment.locale('ru-rp', {
    months: [
        "января", "февраля", "марта", "апреля", "мая", "июня", "июля",
        "августа", "сентября", "октября", "ноября", "декабря"
    ]
});
moment.locale('ru', {
    weekdays : [
        "воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"
    ]
});
moment.locale('ru');

var path = require('path');
//Импортируем все модели. Они будут в переменной models. Например model.user
GLOBAL.models = require("./models")();

//                                              autentification modules
var flash = require("connect-flash"); //флеш сообщения
var passport = require("passport");
var LocalStrategy = require('passport-local').Strategy;
var PassportLocalStrategy = require('passport-local').Strategy;

var cookieParser = require('cookie-parser');
var bodyParser = require("body-parser");
var session = require('express-session');

app.use(cookieParser());// read cookies (needed for auth)

app.use(bodyParser.urlencoded({ extended: false }));// get information from html forms
app.use(bodyParser.json());

app.use(session({
    secret: 'iac_vn',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session

//импорт модели User
var User = require('./models/user.js')();

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.

// Serialize sessions
passport.serializeUser(function(user, done) {
    done(null, user.id);
});
passport.deserializeUser(function(id, done) {
    // query the current user from database
    //User.find(id)
    User.findById(id)
        .then(function(user){
            done(null, user);
        }).error(function(err){
            done(new Error('User ' + id + ' does not exist'));
        });
});

// Use the LocalStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a username and password), and invoke a callback
//   with a user object.  In the real world, this would query a database;

// Use local strategy to create user account
passport.use(new LocalStrategy(
    function(username, password, done) {
        // asynchronous verification, for effect...
        process.nextTick(function () {
            // Find the user by username.  If there is no user with the given
            // username, or the password is not correct, set the user to `false` to
            // indicate failure and set a flash message.  Otherwise, return the
            // authenticated `user`.
            //User.find({where: {username: {$iLike:  username}}}).success(function (user) {
            User.find({where: ["upper(username)=upper('"+username+"')"]}).then(function (user) {
                if (!user) {
                    done(null, false, {message: 'Нет такого пользователя'});
                } else if (password != user.password) {
                    done(null, false, {message: 'Неправильный пароль'});
                } else {
                    done(null, user);
                }
            }).error(function (err) {
                done(err);
            });
        });
    }
));
// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
GLOBAL.ensureAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/node/login')
};
GLOBAL.ensureAuthenticatedAjax = function (req, res, next) {
    if (req.isAuthenticated()) { return next(); }else{
    res.status(401).send('Не авторизовано')};
}
//middleware проверяет роль пользователея
GLOBAL.needsGroup = function(role) {
    return function(req, res, next) {
        //if (req.user && req.user.role === role)
        if (req.user &&  role.indexOf(req.user.role) > -1)
            next();
        else
            res.status(401).send('У вас недостаточно привилегий!');
    };
};

app.get('/node/login', function(req, res){
    res.render('login', { user: req.user, message: req.flash('error')[0] });
});

// POST /login
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
//
//   curl -v -d "username=bob&password=secret" http://127.0.0.1:3000/login
app.post('/node/login',
    passport.authenticate('local', { failureRedirect: '/node/login', failureFlash: true }),
    function(req, res) {

        req.session.userId = req.user.id;
        req.session.ot_musz = req.user.ot_musz;
        req.session.role = req.user.role;

        var adminPageRole = ["administrator" , "vniacstuff"]; //эти пользователи переходят на админскую страницу
        if (adminPageRole.indexOf(req.user.role) > -1) {     //разные группы пользователей - на разные страницы
            res.redirect('/node/admin');
        }else{
            res.redirect('/node/counter');
        }
    });
app.get('/node/logout', function(req, res){
    req.logout();
    res.redirect('/node/login');
});



// соединяемся с сервером Postgres
client.connect(function(err) {
  if(err) {
    return console.error('could not connect to postgres', err);
  }
});

//загрузка всех маршрутов из модулей в папке routers
app.use(require('expressjs.routes.autoload')(path.join(__dirname, './routes'), true));
//приложение для просмотра своих файлов пользователями
require('cute-files')(app, express);

//запуск сервера http и поверх него сервера сокетов
global.io = require('socket.io')
            .listen(app.listen(3000)
                .on("error", function(err){
                    console.log("Caught server error: ");
                    console.log(err.stack)
                }),{ path: '/node/socket.io' }
);

//require('./socket.io/adminsock')();
require('./socket.io/index')();

//app.listen(3000);
//страница приложения http://localhost:3000/
console.log('express3-handlebars server listening on: 3000');




