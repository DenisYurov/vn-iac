vn-iac - web application based on Node.js technology.
Primary goal of application is collect apartment's utility counter values (gas, electricity, heat).
Clients are apartment holders, small companies and individuals.
Client should enter gas, electricity, hot and cold water, and heat counters values every ends of month.
Householder companies have one type account, individual householder another type. 
In household company’s account we can see tables with apartment counters, 
and general whole building counters.  
Application has also administrator account. With this account we can manage other accounts, 
Watch and edit their data, data could be prepared and then download in specific formatted text file.
When data downloaded it will be processed in main desktop billing system for utility bills calculating.

This application has also 2 modules with other purpose.

One of them is file sharing webpage. It allows each LLC user to upload law documents prepared for them individually.
How it works. Layers and accouters prepare documents for each clients. 
Documents saved on local fileserver in folders with client login.
Then main desktop application module goes through those folders, pack files in zip archives with current data in name.
And then it connects with node.js app trough http. Files store in  /sharefiles folder in node.js application
After that clients can download their files in personal account.

Second module takes csv text file and sent it to Telecommunication Company.
Module utilizes telecommunication company HTTPS API.
Result of sending store in database.
Each string of those file is payment and system controls to not send each payment twice.

Application utilize Express framework for uniforms whole application.
Application utilize built in Express ability to serve static content: images, styles, client side javascripts.
Everywhere you can see different middleware connected. 

PostgreSQL was chosen for data storage. 

On top of database we have Sequelize object-relational model module.
Sequelize helps create data models code more readable, especially when program build dynamic complicated queries.

On the other end you can notice Handlebars template processor connected. 
It helps create views clearer, comprehensive, seamless. 
It eliminates string concatenation, have short javascript-like syntax.        

Passport module used for authentication.
Module mount as application middleware and it check authentication each request.

Web application loads all routers automatically from routers folder. No need do 'require' for each router. 

Separatelly I can say about Socket.io module. It realize web socket server.
Socket.io utilized for control calculation and uploading final data on server side. 
This process is time consuming and user can see and control what happen on server side. What is current stage.
Also Socket.io used when many of admin web pages opened. With Socket.io synchronize changes between webpages in this case in real time.

   
