/**
 * Created by dau on 25.11.2015.
 * Загрузка файла на сервер по HTTP, метод PUT параметр filename
 */
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require("path");
module.exports = function(router){
    router.put('/node/upload*', function(request, response) {
        var fileName = request.query.filename||'unnamed.nan'
        var filePath = config.cute_files.rootDir+fileName.toLowerCase();;
        //var dirPath = config.cute_files.rootDir+path.dirname(fileName.toLowerCase());
        winston.log('info', "upload "+ filePath);
        mkdirp(config.cute_files.rootDir+path.dirname(fileName.toLowerCase()),
                function (err) {
                fupload(request, response, filePath)
        });

    });


    function fupload(request, response,filePath) {

        response.writeHead(200);
        var destinationFile = fs.createWriteStream(filePath,{mode: 0777});
        request.pipe(destinationFile);

        var fileSize = request.headers['content-length'];
        var uploadedBytes = 0;
        request.on('data', function (d) {

            uploadedBytes += d.length;
            var p = (uploadedBytes / fileSize) * 100;
            response.write("Uploading " + parseInt(p) + " %\n");

        });

        request.on('end', function () {
            response.end("File Upload Complete");
        });
    }

    return router;
}