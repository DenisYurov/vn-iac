/**
 * Created by dau on 06.04.2015.
 * Страница приветствия. Просто заглушка для пути
 */

module.exports = function(router) {
    router.get('/', function (req, res) {
        res.set({
            'Content-Type': 'text/html',
            'charset': 'UTF-8'
        })
        res.end('Приложение Node JS для vn-iac.ru');
    });
return router;
}