/**
 * Created by dau on 07.04.2015.
 * admin page, tools controller
 */
//импорт моделей
//var counterapp = require('../../models/gilcom.js').getcounterapp();
var User = require('../../models/user.js')();
//var ot_musz = require('../../models/gilcom.js').gettarot_musz();

//отправщик почты
var nodemailer = require('nodemailer');

module.exports =function(router){

    router.get('/node/admin/mailtouser',ensureAuthenticated,needsGroup(['administrator','vniacstuff']),function(req,res){
        //позволяет выполнять несколько запросов одновременно
        //var chainer = new models.Sequelize.Utils.QueryChainer;
        var promiceArr =[];
        promiceArr[0] = User.findById(req.query.user_id);
        promiceArr[1] = models.counterapp.findAll({ limit: 1 });
        Sequelize.Promise.join(promiceArr[0], promiceArr[1], function(resultA, resultB) {
            var transporter = nodemailer.createTransport({
                service: resultB[0].emailaddr1.match("@(.+)\\.")[1], //сервер smtp поле таблицы counterapp
                auth: {
                    user: resultB[0].emailaddr1,     //пользователь и пароль на почтовом сервере. из поля таблицы counterapp
                    pass: resultB[0].emailpassword1
                }
            });
            var mail = {
                from: resultB[0].emailaddr1,
                //results[0].email Нужно вставить на Production!!!!!!  вместо dionisj@mail.ru
                to: resultA.email,
                cc: resultB[0].emailaddr1,
                subject: 'ИАЦ по ЖКХ логин',
                text: resultA.name.trim()+'. Ваш логин: '+resultA.username+' пароль: '+resultA.password+'  Для заполнения показаний счетчиков пройдите http://vn-iac.ru/node/login'
            };

            transporter.sendMail(mail,function(err){
                if(err){
                    console.log(err);
                    //res.end(JSON.stringify(err));
                    JSON.stringify(err)
                }else{
                    res.end('письмо отправлено')
                };

            });
        }).catch(function(error) {
            res.status(422);
            res.end(JSON.stringify(error));
            console.log(error.message === "the error")});
                //отправка напрямую серверу - получателя. должен быть открыт исходящий 25 порт
        /*
        transporter.sendMail({
            from: 'sender@vn-iac.com',
            to: 'debugg81@gmail.com',
            subject: 'hello',
            text: 'hello world!'
        });
        res.end('mail sent');
        */

    });
    //рисуем админскую страницу
    router.get('/node/admin',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req,res){
        models.counterapp.findById(1).then(function(result){
            res.render('counter/admin',{user: req.user.id,isClosedNow: result.isclosednow});
        })
    });

    router.get('/node/admin/usertable',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req, res) {
        if(req.session.role !='administrator') { //не давать не администраторам видеть администраторов в списке users
            var conditions = {
                where: ["role != 'administrator'"]
            }
        } else {var conditions ={}};
        User.findAll(conditions).then(function(User){
            res.json({"data":User})
        });
        /*
        var chainer = new models.Sequelize.Utils.QueryChainer;

        User.findAll()
            .then(function(User){
                for (index = 0; index <User.length; ++index) { //для каждого пользователя находим название управляющей к из ot_musz
                    chainer
                        .add(models.ot_musz.find({ //все запросы к ot_musz отдельные, добавляем их в чейнер
                            where:{ot_musz: User[index].ot_musz }
                            })
                        )
                }
                chainer.runSerially() //порядок выполнения запросов важен
                .success(function(result) {
                        //console.log(result)
                        for (index = 0; index <User.length; ++index) {
                            //User[index].name = result[index].name;
                            if (result[index].hasOwnProperty('name')){
                                //var y = result[index].name
                            };
                            var x = User[index].name;
                            //console.log(User[index].name);
                        };
                        console.log(x);
                        res.json({"data":User})
                    })
                .error(function(errors){
                    res.end(JSON.stringify(errors));
                        console.log('err')
                });

            });
            */
    });
    router.get('/node/admin/getfile',ensureAuthenticated,needsGroup(['administrator']), function(req, res) {
        res.download(appRoot + '/public/file/'+req.query.filename)//отправка файла пользователю
    });
    router.get('/node/admin/usercounter',ensureAuthenticated,needsGroup(['administrator']), function(req, res) {
        req.session.ot_musz=req.query.ot_musz;
        req.session.userId = null;
        res.redirect('/node/counter');
    });
    return router;
}

function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}