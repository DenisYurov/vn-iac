/**
 * Created by dau on 18.05.2015.
 */
/* индивидуальные приборы учета*/
//this module can merge objects
var merge = require('merge');
//шифратор дешифратор
var crypto=require('crypto');
var Q = require('q');//promise library

///node/counters/ipu?ln=d8791ffb85ea7293b7b589bf0a8702b3

module.exports = function(router){
    router.get('/node/counters/ipu', function(req, res) {
        //var chainer = new models.Sequelize.Utils.QueryChainer;
        var lnCipher = req.query.ln;
        var ln = '0';

        //key and iv should be same as the one in mcrypt.php
        var decipher=crypto.createDecipheriv(config.crypto.cipher,config.crypto.key,config.crypto.iv);
        //since we have already added padding while encrypting, we will set autopadding of node js to false.
        decipher.setAutoPadding(false);
        try {
            var dec = decipher.update(lnCipher,'hex','utf8');
            //decrypted data is stored in dec
            dec += decipher.final('utf8'); //возвращает непечатаемые символы в конце
            ln = dec.replace(/[^0-9]+/g,'');
        } catch (ex) {
            winston.log('info', '/node/counters/ipu/cipher lnCipher'+lnCipher);
            res.end('Something wrong with ln');
            return;
        }
        //ln = '265617';

        var data = {month : moment().format("MMMM"),
            year : moment().format("YYYY"),
            monthrp: moment().locale('ru-rp').format("MMMM"),
            closedate: 25,
            ln: ln};
        var counterCondition = {
            where: {ln:ln, sayt : 4},
            include: [
                { model: models.tsg}, // связь к таблице tsg с условием mail5=0 для hv, mail6=0 для gv mail9=0 для en
            ]
        }
        var promiceArr =[];
        promiceArr[0] = models.counterapp.findAll({ limit: 1 });
        promiceArr[1] = models.ener.findAll(counterCondition);
        promiceArr[2] = models.gvod.findAll(counterCondition);
        promiceArr[3] =models.hvod.findAll(counterCondition);
        Sequelize.Promise.join(promiceArr[0], promiceArr[1], promiceArr[2],promiceArr[3],
            function(result1, result2, result3, result4) {
                if (moment().format("DD") > result1[0].daywhenclose || result1[0].isclosednow){ //проверяем закрытие периода
                    data.periodisclosed = true;                                                       //признак закрытости периода
                }else{data.periodisclosed = false;}
                data.closedate = result1[0].daywhenclose;
                var counters = {
                    ener : result2[0],
                    gvod : result3[0],
                    hvod : result4[0]
                }
                res.render('counter/ipu', merge(data,counters));
        }).catch(function(error) {
                res.end('Something wrong')
                console.log(error.message === "the error")});
/*
        chainer
            .add(models.counterapp.findAll({ limit: 1 })) //извлекаем настройки приложения
            .add(models.ener.findAll(counterCondition))
            .add(models.gvod.findAll(counterCondition))
            .add(models.hvod.findAll(counterCondition))
            .run()
            .success(
                function(result) {

                    if (moment().format("DD") > result[0][0].daywhenclose || result[0][0].isclosednow){ //проверяем закрытие периода
                        data.periodisclosed = true;                                                       //признак закрытости периода
                    }else{data.periodisclosed = false;}
                    data.closedate = result[0][0].daywhenclose;
                    var counters = {
                        ener : result[1][0],
                        gvod : result[2][0],
                        hvod : result[3][0]
                    }
                    res.render('counter/ipu', merge(data,counters));
                }
            )
            .error(function(errors){
                    res.end('Something wrong');
            });
        */

    });
    router.get('/node/counters/ipu/savecounter',function(req,res){
        var counter = req.query.data;
        models.ener.find({ where: {ln : counter.ln} })
            .then( function(ener) {

                if (ener) { // if the record exists in the db
                    ener.updateAttributes(counter.ener)
                        .then(function () {
                            gvodupdate (counter,res) //сохраняем горячую воду
                            //res.end('Все счетчики успешно');
                        })
                        .catch(models.Sequelize.ValidationError, function (err) {
                            // respond with validation errors
                            return res.status(422).send(err.errors);
                        });
                } else{gvodupdate (counter,res)}
            })

    });

    function gvodupdate (counter,res){
        models.gvod.find({ where: {ln : counter.ln} })
            .then(function(gvod) {
                if (gvod) { // if the record exists in the db
                    gvod.updateAttributes(counter.gvod)
                        .then(function () {
                            //res.end('успешно');
                            hvodupdate(counter,res) //сохраняем холодную воду
                        })
                        .catch(models.Sequelize.ValidationError, function (err) {
                            // respond with validation errors
                            return res.status(422).send(err.errors);
                        });
                }else{hvodupdate(counter,res)}
            });
    };
    function hvodupdate (counter,res){
        models.hvod.find({ where: {ln : counter.ln} })
            .then(function(hvod) {
                if (hvod) { // if the record exists in the db
                    hvod.updateAttributes(counter.hvod)
                        .then(function () {
                            res.end('Все счетчики успешно сохранены');
                        })
                        .catch(models.Sequelize.ValidationError, function (err) {
                            // respond with validation errors
                            return res.status(422).send(err.errors);
                        });
                }
            });
    };

    return router;

}
//********** end of routers
