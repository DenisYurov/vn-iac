/**
 * Created by dau on 18.06.2015.
 */
var moment = require('moment'),
 Parser = require('node-dbf'),
 path = require('path'),
 Busboy = require('busboy'),
 fs = require('fs');

var dbfPath = path.join(GLOBAL.appRoot,config.dbfimport.tablePath);
module.exports = function (router) {
    router.get('/node/admin/dbfimport',ensureAuthenticated, function (req, res) {

        whatFilesNeed(dbfPath,tablesRename()).catch(function(tablesNeed){return tablesNeed}).done(function(tablesNeed ){
            res.render('counter/dbfimport',{'tables':tablesRename(),'tablesNeed':tablesNeed});
        /*
            forEach(function(tableNeed,i){
          if (tables.indexOf(tableNeed)){
          }*/
        });

    });
    router.get('/node/admin/dbfimport/cleandir',ensureAuthenticated, function (req, res) {
        cleanDIr(dbfPath);
        res.status(200);
        res.json({"success" : "Got command"});
    });
    router.get('/node/admin/dbfimport/whatfilesneed',ensureAuthenticated, function (req, res) {
        //var tables = tablesRename();
        whatFilesNeed(dbfPath,tablesRename())
            .catch(function(tablesNeed){return tablesNeed})
            .done(function(tablesNeed){
            res.json(tablesNeed);
        })
    });
    router.get('/node/admin/dbfimport/convert',ensureAuthenticated, function (req, res) {
        var tableAr = tablesRename();//список таблиц для импорта
        var tablesCount = config.dbfimport.tables.length + config.dbfimport.tables1.length; //число импортируемых таблиц, для прогрессбара
        whatFilesNeed(dbfPath,tableAr)
           .then(function(){//если все таблицы на месте, приступаем к разбору
                io.sockets.emit('progressreset');
                Sequelize.Promise.map(tableAr,function(table){
                tableName = models[table.name];
                return tableName.destroy({truncate:true}).then(function(){
                    var parser = new Parser(path.join(dbfPath,table.fileName));
                    var par = setParser(parser,tableName,tablesCount);
                    return par.parse().promice;
                })
            },{concurrency: 1})
            })
        .catch(function(fileNotExist){   io.sockets.emit('progressinc', {progressinc: '100', message: 'не хватает таблиц: '+fileNotExist.toString()});});
        res.status(200);
        res.json({"success" : "Got command"});
    });
    router.post('/node/admin/dbfimport/upload',ensureAuthenticated, function(req, res) {
        var busboy = new Busboy({ headers: req.headers });
        busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            var saveTo = path.join(dbfPath, path.basename(filename.toLowerCase()));
            file.pipe(fs.createWriteStream(saveTo));
            /*
            file.on('data', function(data) {//собираем пришедший файл в fileBuffer
                //console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            });
            */
            file.on('end', function() { //файл получен
                io.sockets.emit('progressinc', {progressinc: '20', message: 'файл '+fieldname+' получен'});
            });
        });

        busboy.on('finish', function() {
            whatFilesNeed(dbfPath,tablesRename())
                .catch(function(tablesNeed){return tablesNeed})
                .done(function(tablesNeed){
                    io.sockets.emit('progressinc', {progressinc: '100', message: 'Все файлы получены','tablesNeed':tablesNeed});
                    //res.json(tablesNeed);
                })
            //io.sockets.emit('progressinc', {progressinc: '100', message: 'Все файлы получены'});
        });
        req.pipe(busboy);
        res.writeHead(200);
        res.end('iGotCommand');
    });
    return router;
};
function whatFilesNeed(dbfPath,tableAr){ // возвращает массив, файлов которых нехватает в dbfPath по сравнению с tableAr - массивом таблиц из config
    var fs = Sequelize.Promise.promisifyAll(require('fs'));
    return fs.readdirAsync(dbfPath).then(function(files) {//files массив файлов которые сейчас есть в директории
        return new Sequelize.Promise(function(resolve, reject) {
            var needFiles = [];
            tableAr.forEach(function (table, i) {//преобразуем маccив объектов в массив
                needFiles.push(table.fileName)
            });
            fileNotExist = needFiles.filter(function(x) { return files.indexOf(x) < 0 });//есть ли нужные файлы в директории
            if (fileNotExist.length == 0){
                resolve([])
            }else{reject(fileNotExist)};
        })
    })
}
function tablesRename(){ //добавляет месяц и .dbf к названиям таблиц из config
    var mmyy = moment().format("MMYY");
    var tableAr =[];
    //цикл по импортируемым таблицам
    for (var i in config.dbfimport.tables) { //добавляем месяц к названию импортируемой таблицы
        tableAr.push({
            fileName: config.dbfimport.tables[i]+mmyy+'.dbf',
            name: config.dbfimport.tables[i]
        });
    }
    for (var i in config.dbfimport.tables1) {
        tableAr.push({
            fileName: config.dbfimport.tables1[i]+'.dbf',
            name: config.dbfimport.tables1[i]});
    }
    return tableAr
};
function cleanDIr(dirPath){//очистка директории в которой будут таблицы.
    var fs = Sequelize.Promise.promisifyAll(require('fs'));
    return fs.readdirAsync(dirPath).then(function(files){
        Sequelize.Promise.map(files,function(file){
            var filePath = path.join(dirPath,file);
            fs.unlink(filePath)
        }),{concurrency: 5}
    });
}
function setParser(parser,tableName,tablesCount) {//тут самое мясо
    var promiceArr = [];
    var succRecCount = 0; //счетчик успешно добавленных записей
    var totalRecCount = 0; //всего записей
    var progressInd = 0;// для позиции прогрессбара
    var progressInd2 = 0;
    var condition =[];

    parser.on('start', function (p) {
        io.sockets.emit('progressinc', {progressinc: 10  / tablesCount , message: 'старт импорта '+tableName.name});
       // console.log('dBase file parsing has started ');
    });
    parser.on('header', function (h) {
        //console.log('dBase file header has been parsed');
        io.sockets.emit('progressinc', {progressinc: 20 / tablesCount, message: 'разбор таблицы '+tableName.name});
        for (var i in parser.header.fields) { //названия полей переводим в нижний регистр
            parser.header.fields[i].name = parser.header.fields[i].name.toLowerCase()
        };
        progressInd2 = 40/parser.header.numberOfRecords /tablesCount;
    });

    parser.on('record', function (record) {
        ++totalRecCount;
        //корректировка некоторых полей
        var condRec = {};
        for (var fieldName in tableName.attributes) { // проходим по полям модели
             var fieldValue = record[fieldName];
            condRec[fieldName] = fieldValue;
            if (typeof(fieldValue) == 'number' && isNaN(fieldValue)) {
                condRec[fieldName] = 0;
            };
        };
        condition.push(condRec);
        io.sockets.emit('progressinc', {progressinc: progressInd2, message: tableName.name+' разбор тела таблицы '});

    });

    parser.on('end', function (p) {
        progressInd = 40/totalRecCount/tablesCount;
        parser.promice = Sequelize.Promise.map(condition,function(record){
            return tableName.create(record).then(function(){
            ++succRecCount;
            io.sockets.emit('progressinc', {progressinc: progressInd, message: tableName.name+' импорт записей '});
            },{concurrency: 1}).catch(function(err){
                io.sockets.emit('progressinc', {progressinc: progressInd, message: tableName.name+' '+err.message});
            });
        }).done(function(){
            console.log('Усё!');
            io.sockets.emit('progressinc', {progressinc: 100/tablesCount, message: tableName.name+' успешно '+succRecCount+' записей'});
        })

    });

    return parser;
}