/**
 * Created by dionis on 20.04.2015.
 */
var User = require('../../../models/user.js')();
//this module can merge objects
var merge = require('merge')
//var ot_musz = require('../../#databaseBackup/gilcom.js').gettarot_musz();

var rl={roles:[
    {role:'uprcompany', rolename:'Управляющая компания', default:"selected" },
    {role:'administrator', rolename:'Администратор', default:null },
    {role:'vniacstuff', rolename:'Пользователь', default:null }
]};
module.exports = function(router){
    router.get('/node/admin/useredit',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req,res){
        var username = req.query.username.trim();

        User
            .find({where: ["trim(upper(username))=upper('"+username+"')"]})
            //.on('sql', console.log)
            .then(function (user) {
                for (index = 0; index < rl.roles.length; ++index) {
                    if(rl.roles[index].role == user.dataValues.role) {
                        rl.roles[index].default = "selected";
                    }
                }
                res.render('counter/useredit', merge(user.dataValues,rl));
            });
    });

    router.get('/node/admin/usersave',ensureAuthenticated,needsGroup(['administrator']), function(req,res){
        var usr = req.query.data;
        if (usr.ot_musz.length==0){ usr.ot_musz = null } //валидаторы модели хотят null вместо пустой строки
        if (usr.email.length==0){ usr.email = null }
        if (usr.id == undefined||usr.id.length == 0) { //новый юзер*/
        User.create(usr).then(
                function (result) {
                    res.json(result.dataValues.id);
                }).catch(models.Sequelize.ValidationError, function (err) {
                // respond with validation errors
                return res.status(422).send(err.errors);

            });

        }else{
            User.update(usr,{
                where: {id:usr.id}
            })
            .then(
                function (result) {
                    if (result==1){res.json(usr.id)}
                        else{res.status(422);
                        res.json('ошибка')
                    };
                })
            .catch(models.Sequelize.ValidationError, function (err) {
                    // respond with validation errors
                    return res.status(422).send(err.errors);
                });
        };

        /*
        User
            .upsert(usr)
            .then(function(){
                User.findOne(usr).then(
                    function(result){
                        res.json(result.dataValues.id);
                    })
            })
            .catch(models.Sequelize.ValidationError, function (err) {
                // respond with validation errors
                return res.status(422).send(err.errors);
            });
         */

    });
    router.get('/node/admin/useradd',ensureAuthenticated,needsGroup(['administrator']), function(req,res){
         res.render('counter/useredit', merge({id: null},rl));
    });
    router.get('/node/admin/userdel',ensureAuthenticated,needsGroup(['administrator']), function(req,res){
        User.destroy({
            where: {
                id: req.query.id
            }
        }).then(function(){
            res.end('ok')
        }).catch(function(err){
            res.status('422').send(err.errors)
        });

    });
    return router;
}