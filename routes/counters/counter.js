/**
 * Created by dau on 02.04.2015.
 * counter module обслуживает маршруты связанные с домовыми счетчиками для ТСЖ
 */
//модели в глобальной переменной models

//this module can merge objects
var merge = require('merge');
/*
//архиватор
var zip = new require('node-zip')();
*/
//работа с файловой системой
var fs = require("fs");
// импорт не sequelize "model"
var mcounter = require('../../models/sql/mcounter.js');

module.exports = function(router){

    router.get('/node/counter',ensureAuthenticated, function(req, res) {
        var ot_musz = req.session.ot_musz;
        //console.log(req.session.ot_musz +' '+ ot_musz);
        if (ot_musz === "null"){res.end('не может быть счетчика'); return}
        var cSQL = "select otm.name as ot_muz_name from tar.ot_musz otm where otm.ot_musz = $1 limit 1";
        winston.log('info', '/node/counter '+cSQL+' ot_musz:'+ot_musz)
        query = client.query(cSQL,[ot_musz],
            function(err, result) {
                if(err) {
                    return console.error('error running query', err);
                }
                var queryResult = result.rows[0];
                var data = {month : moment().format("MMMM"),
                            year : moment().format("YYYY"),
                            monthrp: moment().locale('ru-rp').format("MMMM"),
                            closedate: 25,
                            user: req.user.id, //for logout
                            admin: false};
                if (req.user.role == 'administrator'){
                    data.admin = true
                }
                //извлекаем настройки приложения асинхронно
                models.counterapp.findAll({ limit: 1 }).then(function(counterapp) {
                    if (moment().format("DD") > counterapp[0].daywhenclose || counterapp[0].isclosednow){ //проверяем закрытие периода
                        data.periodisclosed = true;                                                       //признак закрытости периода
                    }else{data.periodisclosed = false;}
                    data.closedate = counterapp[0].daywhenclose;
                    res.render('counter/counter', merge(data, queryResult));
                    });
            });
    });

    router.get('/node/counters', function(req, res) { //ensureAuthenticated,
        // var ot_musz = req.query.ot_musz;
        var isPrint = req.query.isPrint;
        var ot_musz = req.session.ot_musz;
        var gm = moment().format("YYYYMM");
        //var counter_type = counterTable(req.query.counter_type,moment().format("MMYY"));
        var counterCondition = {
            attributes: [Sequelize.literal('DISTINCT ln'), 'ln','nkw1','nkw2','pred','tek','predn','tekn','knom'],
            where:{sayt: 1},                       //поле sayt 1. счетчик для управляющих компаний
            include: [
                {model: models.haracter, required: true,attributes:['exp_2']},//join к таблице haracter
                {model: models.tsg, required: true, where:{ot_musz: ot_musz},attributes:[]}
            ]
        };
        if (ot_musz == 344 && req.query.counter_type == 'hvod'){ //для водоканала выбираем не так как для всех, переопределяем часть conditions
            counterCondition.where = {sayt: 2}
            counterCondition.include = [
                    {model: models.haracter, required: true,attributes:['exp_2']}
                    ]
        };
        if (ot_musz == 343 && (req.query.counter_type == 'gvod'||req.query.counter_type == 'otop')){ //для теплоэнерго выбираем не так как для всех, переопределяем часть conditions
            counterCondition.where = {sayt: 3}
            counterCondition.include = [
                {model: models.haracter, required: true,attributes:['exp_2']}
            ]
        };
        //выбираем нежилые отдельно
        var table = counterTable(req.query.counter_type);
        var a = table.findAll(counterCondition);
        //для jeu = 9999
        counterCondition.where = [{sayt: 1},{jeu:9999}];
        counterCondition.include = [
            {model: models.haracter, required: true,attributes:['exp_2']},//к таблице haracter
            {model: models.tsg, as:'tsg99', required: true, where:{ot_musz: ot_musz},attributes:[]}
        ];
        var b = table.findAll(counterCondition);
            //.on('sql', console.log)
        Sequelize.Promise.join(a, b, function(resultA, resultB) {
            var data = [];
            if (resultA){
                if (resultB) {
                    result = resultA.concat(resultB)
                };
                result.forEach(function (element, index, array){

                    var record = element.dataValues;
                    record.pred = element.get('pred');
                    record.tek = element.get('tek');
                    record.predn = element.get('predn');
                    record.tekn = element.get('tekn');
                    record.exp_2 = element.dataValues.haracter.dataValues.exp_2;
                    record.haracter = undefined;
                    data.push(record);
                });

            };
            res.json({"data": data});
        });

    });

    router.get('/node/counters/counter/printable',ensureAuthenticated, function(req, res) {
        var ot_musz = req.session.ot_musz;
        var dataFin = {
            month: moment().format("MMMM"),
            year: moment().format("YYYY"),
            sumPred :0, //сюда положим итоги по таблице
            sumTek:0,
            sumPredN :0,
            sumTekN:0,
            counter_type: req.query.counter_type ||'none'
        };
        var counterCondition = {
            attributes: [Sequelize.literal('DISTINCT ln'), 'ln',[Sequelize.literal('CAST (nkw1 AS INTEGER)'),'nkw1'],'nkw2','pred','tek','predn','tekn'],
            where:{sayt: 1},                       //поле sayt 1. счетчик для управляющих компаний
            include: [
                {model: models.haracter, required: true,attributes:['exp_2']},//к таблице haracter
                {model: models.tsg, required: true, where:{ot_musz: ot_musz},attributes:[]}
            ],
            order: [[models.haracter,'exp_2','ASC'],[models.sequelize.cast(models.sequelize.col('nkw1'), 'integer') ,'ASC']]
        };
        var table = counterTable(req.query.counter_type);
        var a = table.findAll(counterCondition);
        //для jeu = 9999
        counterCondition.where = [{sayt: 1},{jeu:9999}];
        counterCondition.include = [
            {model: models.haracter, required: true,attributes:['exp_2']},//к таблице haracter
            {model: models.tsg, as:'tsg99', required: true, where:{ot_musz: ot_musz},attributes:[]}
        ];
        var b = table.findAll(counterCondition)
        //.on('sql', console.log)
        Sequelize.Promise.join(a, b, function(resultA, resultB) {
            var data = [];
            if (resultA){
                if (resultB) {
                    result = resultA.concat(resultB)
                };
                result.forEach(function (element, index, array){
                    //обработка поля из связанной таблицы
                    var record = element.dataValues;
                    record.exp_2 = element.dataValues.haracter.dataValues.exp_2;
                    record.haracter = undefined;

                    dataFin.sumPred += parseFloat(element.pred);
                    dataFin.sumTek += parseFloat(element.tek) ;
                    dataFin.sumPredN += parseFloat(element.predn);
                    dataFin.sumTekN += parseFloat(element.tekn);
                    // пустые графы вместо нулей
                    if (element.tek==0){element.tek=''}
                    if (element.tekn==0){element.tekn=''}

                    data.push(record);
/*
                    var record = element.dataValues;
                    record.pred = element.get('pred');
                    record.tek = element.get('tek');
                    record.predn = element.get('predn');
                    record.tekn = element.get('tekn');
                    record.exp_2 = element.dataValues.haracter.dataValues.exp_2;
                    record.haracter = undefined;
                    data.push(record);
*/
                });

            };
            dataFin.sumPred = dataFin.sumPred.toFixed(4);
            dataFin.sumTek = dataFin.sumTek.toFixed(4);
            dataFin.sumPredN = dataFin.sumPredN.toFixed(4);
            dataFin.sumTekN = dataFin.sumTekN.toFixed(4);
            res.render('counter/cntprintable',merge( dataFin,{"data": data}));
        }).catch(function(error) {
            res.status(422);
            res.end('Something wrong')
            console.log(error.message === "the error")});;
        /*
        table.findAll(counterCondition).then(function(result){
            var data = [];
            if (result){
                result.forEach(function (element, index, array){
                    //обработка поля из связанной таблицы
                    var record = element.dataValues;
                    record.exp_2 = element.dataValues.haracter.dataValues.exp_2;
                    record.haracter = undefined;

                    dataFin.sumPred += parseFloat(element.pred);
                    dataFin.sumTek += parseFloat(element.tek) ;
                    dataFin.sumPredN += parseFloat(element.predn);
                    dataFin.sumTekN += parseFloat(element.tekn);
                    // пустые графы вместо нулей
                    if (element.tek==0){element.tek=''}
                    if (element.tekn==0){element.tekn=''}

                    data.push(record);
                })
            }
            dataFin.sumPred = dataFin.sumPred.toFixed(4);
            dataFin.sumTek = dataFin.sumTek.toFixed(4);
            dataFin.sumPredN = dataFin.sumPredN.toFixed(4);
            dataFin.sumTekN = dataFin.sumTekN.toFixed(4);
            res.render('counter/cntprintable',merge( dataFin,{"data": data}));
        })
            //.on('sql', console.log)
            .catch(function(err){
                res.status(422).send(err.errors)
                return console.error('error running query', err);
            });
*/
/*
            var ot_musz = req.session.ot_musz;
            var gm = moment().format("YYYYMM");
            var counter_type = counterTable(req.query.counter_type,moment().format("MMYY"));
            var mail_type = mailField(req.query.counter_type);
            var cSQL = mcounter.allcounters(counter_type,mail_type);
            // строка запроса вычисляется в модуле ../../models/sql/mcounters.js
            // console.log(mail_type+' '+counter_type +' '+ot_musz+' '+gm);
            winston.log('info', '/node/counters '+cSQL+' ot_musz:'+ot_musz+' gm:'+gm);
            var data = {
                month: moment().format("MMMM"),
                year: moment().format("YYYY"),
                sumPred :0, //сюда положим итоги по таблице
                sumTek:0,
                sumPredN :0,
                sumTekN:0,
                counter_type: req.query.counter_type ||'none'
            };


            query = client.query(cSQL,[ot_musz,gm],
                function(err, result) {
                    if(err) {
                        return console.error('error running query', err);
                    }

                    var queryResult = result.rows;
                    // calculate totals in footer
                    queryResult.forEach(function(entry) {
                        data.sumPred += parseFloat(entry.pred);
                        data.sumTek += parseFloat(entry.tek) ;
                        data.sumPredN += parseFloat(entry.predn);
                        data.sumTekN += parseFloat(entry.tekn);
                        // пустые графы вместо нулей
                        if (entry.tek==0){entry.tek=''}
                        if (entry.tekn==0){entry.tekn=''}
                    });
                    data.sumPred = data.sumPred.toFixed(4);
                    data.sumTek = data.sumTek.toFixed(4);
                    data.sumPredN = data.sumPredN.toFixed(4);
                    data.sumTekN = data.sumTekN.toFixed(4);

                    res.render('counter/cntprintable', merge( data,{"data": queryResult}));
                });
                */
    });
    /*
    router.get('/node/counters/counter/savecntrwarn', function (req, res) {
        var table = counterTable(req.query.counter_type);
        //var msg = table.validateWarn(req.query.attr);
        var msg = table.build(req.query.attr).validateWarn()
        if (msg){
            res.status(422).send(msg)
        }else{
            res.status(200)
        };
    });
    */
    router.get('/node/counters/counter/savecntr',ensureAuthenticatedAjax, function (req, res) {
        var table = counterTable(req.query.counter_type);

        table.find({ where: {ln : req.query.ln} })
            .then( function(result) {
                if (result) { // if the record exists in the db
                    result.updateAttributes(req.query.attr)
                        .then(function () {
                            var msg = result.validateWarn(req.query.attr);//проверяем на предупреждения
                            res.status(200)
                             if (msg) {res.json(msg);}
                            //res.end('Все счетчики успешно');
                        })
                        .catch(models.Sequelize.ValidationError, function (err) {
                            // respond with validation errors
                            return res.status(422).send(err.errors);

                        });
                } else{return res.status(422).send('нет такого счетчика');}
            });

    });

    router.get('/node/modelreload',ensureAuthenticated, function (req, res) {
        //обновление моделей. нужно при появлении в базе таблиц с новыми именами зависящими от текущей даты
        GLOBAL.models = require("../../models")(true);
        //console.log(models.ener.tableName);
        res.end('модели обновлены');
    });
    return router;

}
//********** end of routers


function counterTable(counterType) {
    switch (counterType) {
        case 'hvod':
            return models.hvod;
            break;
        case 'gvod':
            return models.gvod;
            break;
        case 'otop':
            return models.otop;
            break;
        default:
            return models.ener;
    }
};

function mailField(counterType) {
    switch (counterType) {
        case 'hvod':
            return 'mail5 = 1';
            break;
        case 'gvod':
            return 'mail6 = 1';
            break;
        case 'otop':
            return 'mail8 = 1';
            break;
        default:
            return 'mail9 = 1';
    }
};

//дополнение строки слева символами
String.prototype.padLeft = function (paddingChar, length) {

    var s = new String(this);
    if ((this.length < length) && (paddingChar.toString().length > 0))
    {
        for (var i = 0; i < (length - this.length) ; i++)
            s = paddingChar.toString().charAt(0).concat(s);
    }
    return s;
};

