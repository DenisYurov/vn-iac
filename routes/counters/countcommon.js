/**
 * Created by dau on 02.04.2015.
 * counter module обслуживает маршруты связанные с домовыми счетчиками для ТСЖ
 */

//this module can merge objects
var merge = require('merge')

// импорт не sequelize "model"
var mcounter = require('../../models/sql/mcounter.js');

module.exports = function(router){

    router.get('/node/countcommon',ensureAuthenticated, function(req, res) {
        var ot_musz = req.session.ot_musz;
        //берем из базы имя управляющей компании
        var cSQL = "select otm.name as ot_muz_name from tar.ot_musz otm where otm.ot_musz = $1 limit 1";
        query = client.query(cSQL,[ot_musz],
            function(err, result) {
                if(err) {
                    return console.error('error running query', err);
                }
                var queryResult = result.rows[0];
                var data = {month : moment().format("MMMM"),
                            year : moment().format("YYYY"),
                            monthrp: moment().locale('ru-rp').format("MMMM"),
                            closedate: 25,
                            user: req.user.id,
                            admin: false};
                if (req.user.role = 'administrator'){
                    data.admin = true
                }
                //извлекаем настройки приложения асинхронно
                models.counterapp.findAll({ limit: 1 }).then(function(counterapp) {
                    if (moment().format("DD") > counterapp[0].daywhenclose || counterapp[0].isclosednow){ //закрытие периода
                        data.periodisclosed = true;
                    }else{data.periodisclosed = false;}
                    data.closedate = counterapp[0].daywhenclose;
                    res.render('counter/countcommon', merge(data, queryResult));

                    });
            });
    });

    router.get('/node/countcommons',ensureAuthenticated, function(req, res) {
        // var ot_musz = req.query.ot_musz;
        var ot_musz = req.session.ot_musz;
        var gm = moment().format("YYYYMM");
        var counter_type = counterTable(req.query.counter_type/*,moment().format("MMYY")*/);
        var counterTableOdn = counterTableInd(req.query.counter_type/*,moment().format("MMYY")*/);
        var mail_type = mailField(req.query.counter_type);
        var counter_type_older = counterTable(req.query.counter_type,'old'/*moment().subtract(1,'month').format("MMYY")*/);
        //moment().subtract(1,'month').format("MMYY")
        //console.log(counterTableOdn);
        var cSQL = mcounter.countcommons(counter_type, ot_musz, gm, counterTableOdn, mail_type, counter_type_older,req.query.counter_type);//строка запроса вычисляется в модуле ../../models/sql/mcounters.js
        winston.log('info', '/node/countcommons '+cSQL);
        query = client.query(cSQL,
            function(err, result) {
                if(err) {
                    return console.error('error running query', err);
                }
                data = result.rows;
                data.forEach(function(elem,index){
                    data[index].pred = parseFloat(elem.pred).toFixed(4);
                    data[index].tek = parseFloat(elem.tek).toFixed(4);
                    data[index].predn = parseFloat(elem.predn).toFixed(4);
                    data[index].tekn = parseFloat(elem.tekn).toFixed(4);
                });
                res.json({"data":data});
            });

    });
    router.get('/node/savecommoncntr',ensureAuthenticated, function (req, res) {
        //var id = req.query.id;
        var val = req.query.value;
        var iid = req.query.id.substr(0,2);
        var knom = req.query.id.substr(2,req.query.id.length);
        switch (iid) {
            case "t1" : var counterName ="tek";
                break;
            case "p1" : var counterName ="pred";
                break;
            case "t2" : var counterName ="tekn";
                break;
            case "p2" : var counterName ="predn";
                break;
        }
        var cSQL = "UPDATE "+counterTable(req.query.counterType,moment().format("MMYY"))+" set "+counterName+" = "+val+" WHERE knom = " + knom;
        query = client.query(cSQL,
            function(err, result) {
             //   console.log(cSQL+' '+ iid);
                if(err) {
                    return console.error('error running query', err);
                    res.json({"result":"error"});
                }
                res.json({"result":"success"});
            });
    });

    return router;

}

function counterTable(counterType,god) {
    switch (counterType) {
        case 'hv':
            return 'lice.meter_hv'// + god;
            break;
        case 'gv':
            return 'lice.meter_gv'// + god;
            break;
        case 'en':
            return 'lice.meter_en'// + god;
            break;
        case 'ot':
            return 'lice.meter_ot'// + god;
    }
};
function counterTableInd(counterType,god){
        switch(counterType) {
            case 'hv':
                return 'lice.meter_hvod'//+god;
                break;
            case 'gv':
                return 'lice.meter_gvod'//+god;
                break;
            case 'en':
                return 'lice.meter_ener'//+god;
                break;
            case 'ot':
                return 'lice.meter_otop'//+god;
        }
};

function mailField(counterType) {
    switch (counterType) {
        case 'hv':
            return 'mail5 = 1';
            break;
        case 'gv':
            return 'mail6 = 1';
            break;
        case 'ot':
            return 'mail8 = 1';
            break;
        case 'en':
            return 'mail9 = 1';
    }
};