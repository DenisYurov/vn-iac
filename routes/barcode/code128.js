/**
 * Created by dau on 22.04.2015.
 */
//barcode module

var codes = require("rescode");
codes.loadModules(["code128"]);

// codepage converter
//cptable = require('codepage');

module.exports = function(router){
    //генерация штрихкода
    router.get('/node/code128', function (req, res) {
        var data128 = codes.create("code128",req.query.shtk);
        res.setHeader("Content-Type","image/png");
        res.end( data128 );
    })
return router;

}