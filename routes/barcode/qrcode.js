/**
 * Created by dau on 02.04.2015.
 */
//barcode module

var codes = require("rescode");
codes.loadModules(["code128","qrcode"]);

// codepage converter
cptable = require('codepage');

module.exports = function(router){
    router.get('/node/qrcode',function (req, res) {

        var dat = req.query.shtk;
        var data ='no parameter' ;
        if (typeof(dat) !=='undefined'){
            var dt =  cptable.utils.encode(1251, dat, 'str');
            var data = codes.create("qrcode",dt, { "eclevel":"M","parse":true } );
            res.setHeader("Content-Type","image/png");
        }
        res.end( data );
    });
    return router;

}