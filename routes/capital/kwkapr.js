/**
 * Created by dau on 22.04.2015.
 */
/* отдает квитанцию капремонта*/
//подключаемся к базе capital
var pg = require("pg");
var clientCap = new pg.Client(config.postgres.capital_connstring);
//this module can merge objects
var merge = require('merge');
var pdf = require('html-pdf');
var crypto =  require('crypto');

clientCap.connect(function(err) {
    if (err) {
        return console.error('could not connect to postgres', err);
    }
});

module.exports = function(router){
router.get('/node/capital/kwkapr', function (req, res) {
    var ln = req.query.ln;
    var date = req.query.mmyy;
    if (date === undefined){
        date = moment().subtract(1, 'month').format("MMYY");
    }
    var resType = req.query.restype;
    if (req.query.restype === undefined){
        var resType = 'html';
    }else{
        var resType = 'pdf';
    }
    //res.render('kwkapr1214',{});
    //var date ="0115";

    //ln = 11000152;
    //ln = decipherLn(ln);
    var data = {
        shap10: moment(date, "MMYY").date(10).add(1, 'month').format('DD.MM.YYYY'),
        mes   : moment(date, "MMYY").format('MMMM YYYY [года]')
    };
    var l_date = moment(date, "MMYY").format('MMYYYY');
    var queryResult;
    var cSQL = "SELECT * FROM "+'res'+date+" WHERE ln=$1 LIMIT 1";
    /*
    clientCap.connect(function(err) {
        if (err) {
            return console.error('could not connect to postgres', err);
        }
        */
        query = clientCap.query(cSQL, [ln],
            function (err, result) {
                if (err) {
                    return console.error('error running query', err);
                }
                queryResult = result.rows[0];

                //добавляем в queryResult вычисленные поля
                queryResult.QRshtk = 'ST00011|Name='+'СНКО "Региональный фонд"'+'|PersonalAcc='+queryResult.rs.trim()+
                '|BankName='+queryResult.bank.trim() +'|BIC='+queryResult.bik.trim()+'|CorrespAcc='+queryResult.ks.trim()+
                '|PayeeINN=5321801523|KPP=532101001|SUM='+queryResult.asum.trim().replace(".","")+'|PersAcc='+
                '0'+queryResult.ln.trim().substr(0,8)+
                '|PaymPeriod='+l_date+'|AddAmount=000';
                //adres
                queryResult.adres = 'Новгородская область, ' + (queryResult.locality != 1 ? queryResult.raj + 'район, ' : "") +
                queryResult.locatype.trim() + ' ' + queryResult.punkt.trim() + ', ' + queryResult.short.trim() + ' ' +
                queryResult.namekyl.trim() + ',  д.' + queryResult.ndom1.trim();
                //adres1
                if (queryResult.ndom2.trim() != '') {
                    queryResult.adres1 = '/' + queryResult.ndom2.trim()
                } else {
                    if (queryResult.krp.trim() != '') {
                        queryResult.adres1 = '.корп.' + queryResult.krp.trim()
                    } else {
                        queryResult.adres1 = ''
                    }
                }
                queryResult.adres1 = queryResult.adres1 + ', кв.' + queryResult.nkw1.trim() + (queryResult.nkw2.trim() != "" ? ', ком.' + queryResult.nkw2.trim() : '');
                //Укорачиваем ln и добавляем слева нули до 9 символов
                queryResult.lnShort = ("000000000" + queryResult.ln.slice(-8)).slice(-9)
                //("0000" + n).slice(-4)
                // объединяем объекты data, queryResult и отправляем в шаблон - отрисовывать
                //res.render('capital/kwkapr0315', merge(data, queryResult));

                res.render('capital/kwkapr0315', merge(data, queryResult),function(err, html){
                    //var fs = require('fs');
                    //var filePath = appRoot + '/public/file';
                    //var options = {format: 'Letter'};
                    if (resType == 'pdf'){
                        pdf.create(html,config.phantom).toStream(function(err, stream){
                            res.type('application/pdf');
                            stream.pipe(res);
                        });
                    }else{
                        res.writeHead(200, {'Content-Type': 'text/html'});
                        res.write(html);
                        res.end();
                    }
                });
            });
    });
//phantom test
    /*
    router.get('/node/capital/test', function (req, res) {
        var fs = require('fs');
        var pdf = require('html-pdf');
        var filePath = appRoot + '/public/file';
        var html = fs.readFileSync(filePath+'/businesscard.html', 'utf8')
        var options = {format: 'Letter'};

        pdf.create(html, options).toFile(filePath+'/businesscard.pdf', function (err, res) {
            if (err) return console.log(err);
            console.log('ready '+res); // { filename: '/app/businesscard.pdf' }
        });
    });
*/
    return router;

}
//********** end of routers

//дополнение строки слева символами
String.prototype.padLeft = function (paddingChar, length) {

    var s = new String(this);
    if ((this.length < length) && (paddingChar.toString().length > 0))
    {
        for (var i = 0; i < (length - this.length) ; i++)
            s = paddingChar.toString().charAt(0).concat(s);
    }
    return s;
};
function decipherLn(lnCipher) {

    var ln = '0';

//key and iv should be same as the one in mcrypt.php
    var decipher = crypto.createDecipheriv(config.crypto.cipher, config.crypto.key, config.crypto.iv);
//since we have already added padding while encrypting, we will set autopadding of node js to false.
    decipher.setAutoPadding(false);
    try {
        var dec = decipher.update(lnCipher, 'hex', 'utf8');
        //decrypted data is stored in dec
        dec += decipher.final('utf8'); //возвращает непечатаемые символы в конце
        ln = dec.replace(/[^0-9]+/g, '');
    } catch (ex) {
        winston.log('info', '/node/counters/ipu/cipher lnCipher' + lnCipher);
        //res.end('Something wrong with ln');
        return;
    }
    return ln;
};
