/**
 * Created by dau on 29.01.2016.*
 * маршрут для отправки текстового файла в Ростелеком.
 **/
var rostelecom = require('../../modules/rostelecom.js');//модуль с функциями общения с ростелекомом
var Busboy = require('busboy');
var moment = require('moment');
var Q = require('q');
Sequelize = require('sequelize');
//var Payment = require('../models/rostelecom.payment.js')();
module.exports = function(router){


    router.get('/node/rostelecom',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req, res) {
        res.render('rostelecom/index');
    });
    //тест
    router.get('/node/rostelecom/test', function(req, res) {
    rostelecom.checkPaymentParams(0,'75554000001',undefined,100,'тест',function (result) {
        console.log(result);
        res.json(result)
        });
    });

//delete all payments
    router.get('/node/rostelecom/cancel', function(req, res) {
        models.payment.findAll({
            where: {payment_id: req.query.ids},
            attributes: [['payment_id', 'srcPayId']]
        }).then(function (result) {
            var promiceArr = [];
            var sUccess = 0;
            var progressInd = 100 / result.length;//вычисление шага прогрессбара
            result.forEach(function (value, index, ar) { //идем по массиву платежей
                promiceArr.push(rostelecom.abandonPayment(value.dataValues).then(
                    function (data) {
                        var paymt = data.resBody;
                        if (paymt.reqStatus == '0'||'1') { //в ответе от сервера на запрос reqStatus = 0 значит платеж отменен успешно, '1' такого платежа нет в ЕСПП
                            io.sockets.emit('progressinc', {progressinc: progressInd , message: 'удален payment_id: '+value.dataValues.srcPayId});
                            return models.payment.update( // меняем статус платежа в таблице платежей, возвращаем promice для promiceArr
                                {status_id: -1, lastReqNote: paymt.reqNote},
                                {where: {payment_id: value.dataValues.srcPayId}}
                            )//.then(function(){console.log('1')})
                        }else{
                                io.sockets.emit('progressinc', {progressinc: progressInd , message: paymt.reqNote})
                        }
                    }
                ).catch(function (err) {
                        io.sockets.emit('progressinc', {progressinc: progressInd , message: 'ошибка удаления платежа' + JSON.parse(err.toString())})
                    }))

            });

            var allPromise = Q.all(promiceArr);
            allPromise.then(function(data){
                io.sockets.emit('progressfinish')
            });
        });
        res.writeHead(200);
        res.end('iGotCommand');
    });
    router.get('/node/rostelecom/ourdata',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req, res) { //данные для таблицы с платежами отправленными с нашей стороны
        var condition = {
            where:{$or:[{payTime:{$between: [moment().subtract(4, 'weeks').format("YYYY-MM-DDTHH:mm:ss+6:00"),moment().format("YYYY-MM-DDTHH:mm:ss+6:00")]}},
                        {status_id:0}]
            },
            include : [{
                model: models.payment_status, attributes:['name']
            }]
        };
        var data =[];
        models.payment.findAll(condition).then(function(Payment){
            Payment.forEach(function (element, index, array){
                var record = element.dataValues;
                record.status = element.dataValues.payment_status.dataValues.name;
                record.payment_status = undefined;
                data.push(record);
            });
            res.json({"data":data})
        });
    });

//загрузка файла в базу
    router.post('/node/rostelecom/upload',ensureAuthenticated,needsGroup(['administrator','vniacstuff']), function(req, res) {
        var busboy = new Busboy({ headers: req.headers });
        var fileBuffer = new Buffer('');
        busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

            file.on('data', function(data) {//собираем пришедший файл в fileBuffer
                fileBuffer = Buffer.concat([fileBuffer, data]);
            });

            file.on('end', function() {
                io.sockets.emit('progressinc', {progressinc: '20', message: 'файл получен'});
                //разбор текстового файла
                var text = fileBuffer.toString();
                var begin = text.split("END");//делим файл до слова END
                var splitted = begin[0].split("\n");//делим файл на строки
                var promiceArr=[];//массив обещаний
                var progressInd = 80 / splitted.length;//вычисление шага прогрессбара
                var sucAdd = 0;//Счетчик успешно добавленых в базу платежей
                splitted.forEach(function (element, index, array){
                    //разбор строки
                    var datePaymentM = moment(element.substring(64,83), "DD.MM.YYYY HH:mm:ss")// +'+06:00' .utcOffset("+06:00");//18:50:21
                    var data ={
                        svcNum : element.substring(32,44),
                        payAmount : element.substring(22,28)+element.substring(29,31),//element.substring(52,60),
                        payTime: datePaymentM.format("YYYY-MM-DDTHH:mm:ss+6:00"),
                        status_id:0
                    };

                    //var datePayment = element.substring(64,74);
                    //var timePayment = element.substring(75,83);
                    if (data.payTime !== 'Invalid date') {
                        promiceArr.push( rostelecom.insertFreshPayment(data).then(function(){
                                io.sockets.emit('progressinc', {progressinc: progressInd , message: 'добавление в базу'});
                                sucAdd++;
                            })
                        )

                    } else {
                        io.sockets.emit('progressinc', {progressinc: progressInd , message: 'неправильная дата '+data.svcNum});
                    }

                    io.sockets.emit('progressinc', {progressinc: progressInd , message: 'обработка '+data.svcNum});
                      //  .on('sql', console.log);
                });
                Sequelize.Promise.all(promiceArr)
                    .then(function() {
                        io.sockets.emit('progressinc', {progressinc: 100 , message: 'Добавлено ' + sucAdd+' из '+splitted.length +' строк'})
                    }).catch(function(err){
                        io.sockets.emit('progressinc', {progressinc: 100 , message: 'ошибка разбор файла' + err.toString()})
                    }).done(io.sockets.emit('progressfinish'));
            });

        });

        busboy.on('finish', function() {});

        req.pipe(busboy);
        res.writeHead(200);
        res.end('iGotCommand');
    });


    router.get('/node/rostelecom/sendfresh', function(req, res) {
        readNSendFreshPayment ();
        res.writeHead(200);
        res.end('iGotCommand');
    });

    return router;
};
function checkLine(str){ //проверка строки в файле платежа
    var patt = new RegExp("^/d{2}");
    return patt.test(str);
};
function readNSendFreshPayment () { // читаем из базы все платежи со статусом 0
//    rostelecom.readFreshPayment().then(result);

    rostelecom.readFreshPayment()
        .then(function(result) {
            var progressInd = 100 / result.length;
            var promiceArr=[];
            var sucSend = 0;
            var totalFresh = result.length;
            result.forEach(function (value, index, ar) {
                var dataPost = {
                    srcPayId: value.payment_id,
                    svcNum: value.svcNum.trim(),
                    payAmount: value.payAmount,
                    payTime: value.payTime.trim(),
                    reqTime: moment().format("YYYY-MM-DDTHH:mm:ss+6:00")//'2016-02-03T12:43:15+6:00'
                };

                //создаем массив promice
                promiceArr.push(rostelecom.createPayment(dataPost).then(
                     function (result) { //отправляем платеж в ЕСПП
                        var paymt = result.resBody

                        if (paymt.reqStatus == 0) { //в ответе от сервера на запрос reqStatus = 0 значит платеж зачислен успешно

                            result.payment.update( // меняем статус платежа в таблице платежей
                                {status_id: 1},
                                {where: {payment_id: paymt.srcPayId}}
                            );
                            sucSend++;//прогрессбар на фронтэнде
                            io.sockets.emit('progressinc', {progressinc: progressInd , message: 'отправлен '+dataPost.svcNum});
                        }else { // платеж не зачислен
                            io.sockets.emit('progressinc', {
                                progressinc: progressInd,
                                message: 'ошибка ' + dataPost.svcNum + ' ' + paymt.reqNote
                            });
                            models.request_status.findOne({where: {id: paymt.reqStatus}}) //ищем описание причины отклонения платежа
                                .then(function (request_status) {
                                    var note = request_status.id +' '+ request_status.payStatus.trim();
                                    result.payment.update( // меняем поле описания причины отклонения платежа в таблице платежей
                                        {lastReqNote: note},
                                        {where: {payment_id: dataPost.srcPayId}}
                                );
                            });
                        };
                        result.request.create({ //добавляем запись в таблицу с историей запросов
                        payment_id: paymt.srcPayId,
                        reqTime: paymt.reqTime,
                        reqType: paymt.reqType,
                        reqStatus: paymt.reqStatus,
                        payStatus: paymt.payStatus,
                        reqNote: paymt.reqNote
                    });

                    }).catch(function (err) {
                        io.sockets.emit('progressinc', {progressinc: 100 , message: 'ошибка отправки' + JSON.parse(err.toString())})
                    })
                );
           });
            var allPromise = Q.all(promiceArr);

            allPromise.then(function(data){
                    io.sockets.emit('progressinc', {progressinc: 100 , message: 'успешно '+sucSend +' из '+totalFresh})
            }).catch(function (err) {
                    io.sockets.emit('progressinc', {progressinc: 100 , message: 'ошибка отправки' + JSON.parse(err.toString())})
            }).done(io.sockets.emit('progressfinish'));

        }
    ).catch(function (err) {
            io.sockets.emit('progressinc', {progressinc: 100 , message: 'ошибка чтения из базы' + JSON.parse(err.toString())})
        });


};
