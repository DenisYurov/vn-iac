/**
 * Created by dau on 22.05.2015.
 */
var zip = new require('node-zip')();
//работа с файловой системой
var fs = require("fs");
//перекодировка кодовых страниц
cptable = require('codepage');
//var iconv = require('iconv-lite');

//    module.exports = admin;
//function countersToZipFile
module.exports = function  (ipu,socket) {
    //выгрузка счетчиков в виде zip архива
    //socket.emit('progressclean');
    //socket.emit('progressinc', {progressinc: '25', message: 'запуск'});
    //var ipu = false; //индивидуальные выгружаем или общедомовые
    //var chainer = new models.Sequelize.Utils.QueryChainer;
    var zip = new require('node-zip')();
    var endofline = '\n';
    var counterString = '';
    var counterCondition = {
        
        where: models.Sequelize.or(
            {tek: {gt: 0}},                       //поле tek больше 0. счетчик заполнен
            {tekn: {gt: 0}}
        ),

        include: [
            {model: models.haracter, required: true}//к таблице haracter
        ]
    };
    /*
    if (!ipu) {
        counterCondition.include.push({model: models.tsg, required: true}) //required: true связь Inner join к таблице tsg
    } else {
        counterCondition.include.push({model: models.tsg, as: 'tsgipu', required: true})//тут учитывается поле mail
    }
*/
    //добавляем запросы в связку
    //winston.log('info', 'Start query');

    var promises = [];

    promises.push(models.ener.findAll(counterCondition).then(function(result) {
        socket.emit('progressinc', {progressinc: '10', message: 'gvod'});
        return result
    }));

    promises.push(models.gvod.findAll(counterCondition).then(function(result) {
        socket.emit('progressinc', {progressinc: '10', message: 'gvod'});
        return result
    }));

    promises.push(models.hvod.findAll(counterCondition).then(function(result) {
        socket.emit('progressinc', {progressinc: '10', message: 'hvod'});
        return result
    }));

    promises.push(models.otop.findAll(counterCondition).then(function(result) {
        socket.emit('progressinc', {progressinc: '10', message: 'otop'});
        return result
    }))

    if (!ipu) {

        promises.push(models.en.findAll(counterCondition).then(function(result) {
            socket.emit('progressinc', {progressinc: '10', message: 'en'});
            return result
        }));

        promises.push(models.gv.findAll(counterCondition).then(function(result) {
                socket.emit('progressinc', {progressinc: '10', message: 'gv'});
                return result
            }));
        promises.push(models.hv.findAll(counterCondition).then(function(result) {
                socket.emit('progressinc', {progressinc: '10', message: 'hv'});
                return result
            }));

        promises.push(models.ot.findAll(counterCondition).then(function(result) {
            socket.emit('progressinc', {progressinc: '10', message: 'ot'});
            return result
        }));
        
    };

    Sequelize.Promise.all(promises).then(
        function (results) {
            socket.emit('progressinc', {progressinc: '10', message: 'запросы закончены'});
            for (var j in results) {         //идем по типам счетчиков
                for (var i in results[j]) { //идем по счетчикам одного типа
                    var result = results[j][i];
                    if (result.nkw1 === undefined){
                        result.nkw1 =''
                    };
                    if (result.nkw2 === undefined){
                        result.nkw2 =''
                    };
                    var firstStr = result.get('vidnach') + result.jeu.toString().padLeft(" ", 4) + moment().format("MMYYYY") + result.ln.toString().padLeft("0", 6) +
                        result.nkw1.toString().padLeft(" ", 4) + "    "/*result.nkw2.toString().padLeft(" ", 4)*/;
                    var secondStr = '0000000.000000' + '00'
                    if (!ipu) { //не индивидуальные приборы учета
                        //secondStr = secondStr + result.tsg.ot_musz.padLeft(" ", 3)
                        secondStr = secondStr + '   ';
                    } else {              //индивидуальные приборы учета
                        //secondStr = secondStr + result.tsgipu.ot_musz.padLeft(" ", 3)
                        secondStr = secondStr + '   ';
                    }
                    secondStr = secondStr + '                 ' + '0000000.0000 ' + result.haracter.exp_2.padLeft(" ", 80) + endofline;
                    counterString = counterString + firstStr +
                    result.get('pred').toString().padLeft(" ", 12) + result.get('tek').toString().padLeft(" ", 12) + '1' + secondStr;
                   // try {
                        if (result.vidnach == '09' || result.ln != '0') {    //1 строка со счетчиками только по общедомовым холодной и горячей
                            counterString = counterString + firstStr +
                            result.get('predn').toString().padLeft(" ", 12) + result.get('tekn').toString().padLeft(" ", 12) + '2' + secondStr;
                        }
                   // } catch (e) {
                   //     console.log(e);
                   // };
                }
            }

            socket.emit('progressinc', {progressinc: '10', message: 'архивируем'});
            console.log('архивируем');
            if (!ipu) {
                var fNPrefix = 'upr';
            } else {
                var fNPrefix = 'ind';
            }
            //cptable.utils.encode(866,counterString)
            zip.file(fNPrefix + moment().format("MMYY") + '.txt',  counterString);
            var data = zip.generate({type: 'string'});
            var filePath = appRoot + '/public/file/'+fNPrefix+'counters.zip'
            //удаляем файл если он есть зачем то асинхронно
            fs.unlink('filePath', function (err) {
                fs.writeFileSync(filePath, data, 'binary');
                socket.emit('progressinc', {progressinc: '100', message: 'файл готов'});
                socket.emit('file', {filename: fNPrefix+'counters.zip'});
            });

        }).catch(function (errors) {
            console.log(errors);
            socket.emit('progressinc', {progressinc: '0', message: 'ошибка'});

            winston.log('adminsock.js:107', JSON.stringify(errors));
        });

}