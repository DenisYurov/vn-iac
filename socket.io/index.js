/**
 * Created by dau on 25.02.2016.
 */
//require('./adminsock')();
module.exports = function () {

    io.sockets.on('connection', function (socket) {
        socket.on('getCounters', function (data) {
            //countersToZipFile(data.ipu,socket);
            require('./adminsock')(data.ipu,socket);
        });
        socket.on('isClosedNow', function (data) { //сообщения при нажатии закрытия периода
            models.counterapp.update({
                isclosednow: data.isClosedNow
            }, {
                where:{id: 1}
            }).then(function(result){
                socket.broadcast.emit('closePeriod', {isClosedNow: data.isClosedNow});}) 
        });

    });

}
