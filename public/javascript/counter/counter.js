/*
function tableTotal(col,api){
    var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
            typeof i === 'number' ?
                i : 0;
    };

    // Total
    var total = api
        .column( col )
        .data()
        .reduce( function (a, b) {
            var x = Number(a) + Number(b);
            //console.log(x);
            return x.toFixed(4);
        },0 );
    // Update footer
    $( api.column( col ).footer() ).html(
        total
    );

};
*/
function zeroTekCnt(api){

    // count
    var total = api
        .data()
        .reduce( function (a, b) {
            var x = Number(a);
            if (Number(b.pred)>0 && Number(b.tek) == 0){
                x = Number(a) + 1;
            }

            return x;
        },0 );
    // Update footer api.column( col ).footer(2)
    $(api.table().footer())[0].rows[0].cells[4].innerHTML =
        total
    ;

};
function zeroTeknCnt(api){

    // count
    var total = api
        .data()
        .reduce( function (a, b) {
            var x = Number(a);
            if (Number(b.predn)>0 && Number(b.tekn) == 0){
                x = Number(a) + 1;
            }

            return x;
        },0 );
    // Update footer api.column( col ).footer(2)
    var cell6=$(api.table().footer())[0].rows[0].cells[6]
    if (cell6) {
        cell6.innerHTML =
            total
    };

};

function homeCalc(api){
//подсчет итогов с группировкой по домам.
// возвращает массив groupTotal[knom] = {total1: total1, total2: total2, firstIndex: firstIndex, lastIndex: i - 1};

    var rows = api.rows().nodes();
    var dat = api.rows().data();
    var last=api.column(1).data()[0];
    var firstIndex = 0;
    var total1 = 0, total2 = 0;
    var groupTotal = [];

    api.column(1).data().each( function ( group, i ) {
        if (last !== group) {
            var knom = api.column(8).data()[i - 1];
            groupTotal[knom] = {total1: total1.toFixed(4), total2: total2.toFixed(4), firstIndex: firstIndex, lastIndex: i - 1};
            total1 = 0;
            total2 = 0;
            last = group;
            firstIndex = i;

        }
        if (parseFloat(dat[i].tek) > 0) {
                total1 = total1 + parseFloat(dat[i].tek) - parseFloat(dat[i].pred);
            };
        if (parseFloat(dat[i].tekn) > 0) {
                total2 = total2 + parseFloat(dat[i].tekn) - parseFloat(dat[i].predn);
            };
} );
    var i = api.column(1).data().length;
    var knom = api.column(8).data()[i-1];
    groupTotal[knom] = {total1: total1.toFixed(4), total2: total2.toFixed(4), firstIndex: firstIndex, lastIndex: i-1 };

    return groupTotal;

};
function homeCalc1(api,index){ //подсчет итого по дому по индексам в api.rows().data()

    var dat = api.rows().data();
    var total1 = 0, total2 = 0;
    var groupT =[];
    for (i = index.firstIndex; i<=index.lastIndex; i++) {
        if (parseFloat(dat[i].tek) > 0) {
            total1 = total1 + parseFloat(dat[i].tek) - parseFloat(dat[i].pred);
        };
        if (parseFloat(dat[i].tekn) > 0) {
            total2 = total2 + parseFloat(dat[i].tekn) - parseFloat(dat[i].predn);
        };
        groupT = {total1: total1.toFixed(4), total2: total2.toFixed(4), firstIndex: index.firstIndex, lastIndex: index.lastIndex};
    };
    return groupT;

}
