/**
 * Created by dau on 18.05.2015.
 */

//обработка ошибок ввода
function errorfun(jqXHR){
    var err = $.parseJSON(jqXHR.responseText)[0];
    if (err.type == "Validation error"){
        errinput('#'+err.path, err.message);
    }else{
        errsend('Ошибка сервера! Данные не сохранены')
    };

}
// вывод alert
function errsend(text) {
    $("#alert").fadeIn("slow");
    $("#alerttxt").text(text);
    setTimeout(
        function() { $("#alert").fadeOut("slow"); },
        4000
    );
};

function errinput(elem,text) {
    var jElement = $(elem);
    jElement.addClass('invalid');
    setTimeout(
        function() { jElement.removeClass('invalid'); elem.value = elem.defaultValue;},
        5000

    );
    $("#alert").fadeIn("slow");
    $("#alerttxt").text(text);
    setTimeout(
        function() { $("#alert").fadeOut("slow"); },
        4000
    );
};

function warninput(elem,text) {
    var jElement = $(elem);
    jElement.addClass('warning');
    setTimeout(
        function() { jElement.removeClass('warning'); },
        5000

    );
    $("#message").fadeIn("slow");
    $("#messagetxt").text(text);
    setTimeout(
        function() { $("#message").fadeOut("slow"); },
        4000
    );
};
function myMsg(text) {
    $("#message").fadeOut("fast");
    $("#message").fadeIn("slow");
    $("#messagetxt").text(text);

    setTimeout(
        function() { $("#message").fadeOut("slow"); },
        4000
    );
};