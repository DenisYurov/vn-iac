/**
 * Created by dau on 03.06.2015.
 */
function doGetCaretPosition (oField) {

    // Initialize
    var iCaretPos = 0;

    // IE Support
    if (document.selection) {

        // Set focus on the element
        oField.focus ();

        // To get cursor position, get empty selection range
        var oSel = document.selection.createRange ();

        // Move selection start to 0 position
        oSel.moveStart ('character', -oField.value.length);

        // The caret position is selection length
        iCaretPos = oSel.text.length;
    }

    // Firefox support
    else if (oField.selectionStart || oField.selectionStart == '0')
        iCaretPos = oField.selectionStart;

    // Return results
    return (iCaretPos);
};

function makeProgress(elem) { //обвязка прогрессбара
    var currentCount = 0;
    return { // возвратим объект
        getnext: function (data) {
            //console.log(currentCount+ data.progressinc);
            currentCount = parseFloat(currentCount) + parseFloat(data.progressinc);
            if (currentCount > 100) {
                currentCount = 100
            }
            ;
            $(elem).width(parseInt(currentCount) + "%");
            $(elem).text(data.message + ' ' + parseInt(currentCount) + "%");
        },
        reset: function () {
            currentCount = 0;
            $(elem).width('0%');
            $(elem).text("");
        },
        val: function () {
            return currentCount
        }
    }
}
;
