/**
 * Created by dau on 25.01.2016.
 */
var fs = require('fs');
var https = require('https');
var querystring = require('querystring');
var key = fs.readFileSync(GLOBAL.appRoot + config.rostelecom.keyPath);
var cert = fs.readFileSync(GLOBAL.appRoot + config.rostelecom.crtPath);
//var ca = fs.readFileSync(GLOBAL.appRoot + config.rostelecom.caPath);
var Payment = require('../models/rostelecom.payment.js')();
var Request = require('../models/rostelecom.request.js')();
var Q = require('q'); //promise library
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // fix deep-zero-error

exports.checkPaymentParams = function (svcTypeId,svcNum,svcSubNum,payAmount,payComment,callback){
    /* example
    var dataPost = {
        agentAccount: 4516,
        reqType: 'checkPaymentParams',
        svcTypeId: 0,
        svcNum: 75554000001,
        svcSubNum: 1,
        payCurrId: 'RUB',
        payAmount: 100,
        payPurpose: 0,
        payComment: 'тест'
    }
    */

    var dataPost = {
        agentAccount: config.rostelecom.agentAccount,
        agentId: config.rostelecom.agentId,
        reqType: 'checkPaymentParams',
        svcTypeId: svcTypeId|0,
        svcNum: svcNum ,
        svcSubNum: svcSubNum | '',
        payCurrId: 'RUB',
        payAmount: payAmount|0,
        payPurpose: 0,
        payComment: payComment|''
    }
    postTelecom(dataPost,callback);
};

exports.createPayment = function (dataPost,callback){

  /* example
   agentAccount=4516&reqType=createPayment&svcTypeId=RT.NW.53.ACCOUNT_NUM&svcNum=00052:000000&payAmount=.38
   &srcPayId=0&reqTime=2016-01-12T10:57:15+6:00&payTime=2016-01-12T10:57:15+6:00
   &payCurrId=RUB&payComment=test"
   */
    dataPost.agentAccount = config.rostelecom.agentAccount;
    dataPost.agentId = config.rostelecom.agentId,
    dataPost.reqType = 'createPayment';
    dataPost.svcTypeId = config.rostelecom.svcTypeId;
    dataPost.payCurrId = 'RUB';
//({resBody: function(resBody){querystring.parse(resBody)}}
   // postTelecom(dataPost,callback);
    return postTelecom(dataPost);
};
exports.abandonPayment = function (attr) {
    //'reqType=abandonPayment&srcPayId=1237734555&payTime=2011-10-25T13%3A23%3A15%2B6%3A00&agentAccount=1234'
    //agentAccount=4516&reqType=abandonPayment&srcPayId=7080&payTime=2016-03-15T11%3A07%3A17%2B6%3A00
    return postTelecom({
        //agentAccount: config.rostelecom.agentAccount,
        agentId: config.rostelecom.agentId,
        reqType: 'abandonPayment',
        srcPayId: attr.srcPayId,
        reqTime:  moment().format("YYYY-MM-DDTHH:mm:ss+6:00")

    });
};
exports.getPaymentsStatus = function (dataPost,callback) {
//reqType=getPaymentStatus&statusType=1$agentAccount=1234
    dataPost.agentAccount = config.rostelecom.agentAccount;
    dataPost.agentId = config.rostelecom.agentId,
    dataPost.reqType = 'getPaymentsStatus';
    //var dataPost ={reqType:'getPaymentStatus',endDate:'2016-01-29T15:13:15+6:00'}
    postTelecom(dataPost,callback);
};

//work with database
exports.readFreshPayment = function(callback) {

    var conditions = {
        where: ["status_id = 0"]
    }
    return Payment.findAll(conditions);
};
exports.insertFreshPayment = function(data) {//вставляем новый платеж в базу
    return Payment.create(data)
};


function postTelecom(dataPost,callback) {    // формирование запроса к веб сервису Ростелекома
    // dataPost объект данных для post запроса

    //  process.stdout.write(d); like console.log
    var options = config.rostelecom.http
        options.key = key;  //fs.readFileSync(GLOBAL.appRoot + '/ssl/vniackey.key'),
        options.cert = cert; //fs.readFileSync(GLOBAL.appRoot + '/ssl/vniacquery.crt'),
        //options.ca = ca;
    options.rejectUnauthorized = false;        // Deep_zero_error https://github.com/request/request/issues/418

    //options.requestCert = true;
    //options.agent = false;

    var deferred = Q.defer();
    var req = https.request(options, function (res) {

        //function(res) is called when the connection is established
        res.setEncoding('utf8');
        var resBody ="";
        res.on('data', function (d) {   //res.on('data') is called when there's a chunk of data (this almost certainly will be more than once)
            resBody += d;
        });
        res.on('end', function() { //res.on('end') is called when the connection closes.
            var resBodyP = querystring.parse(resBody);// из строки параметров в объект
            deferred.resolve({statusCode:  res.statusCode, headers: res.headers, 'resBody': resBodyP, request: Request, payment: Payment });

        })
    });

    var dataString = querystring.stringify(dataPost);// Build the post string from an object

    req.write(dataString);//тут передача параметров POST запроса
    req.end();
    req.on('error', function (err) {
        console.log(err);
        deferred.reject(err);
    });
    return deferred.promise.nodeify(callback);
};

